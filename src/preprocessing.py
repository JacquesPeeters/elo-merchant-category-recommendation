import numpy as np
import pandas as pd
import datetime

import src.utils as utils # noqa


def preprocessing_merchants(merchants):
    merchants = merchants[[
        'merchant_id', 'merchant_group_id', 'category_4']].copy()
    merchants = merchants.drop_duplicates('merchant_id')
    for col in ['category_4']:
        merchants[col] = merchants[col].map({'Y': 1, 'N': 0})

    fe_merchants = (merchants.groupby('merchant_group_id').agg({'merchant_id': 'count'}).reset_index())
    fe_merchants = fe_merchants.rename(columns={'merchant_id': 'merchant_group_id_count'})
    merchants = pd.merge(merchants, fe_merchants, how='left')

    return merchants


def preprocessing_transactions(transactions, merchants, PROTOTYPING=False):
    nrow_begin = transactions.shape[0]
    transactions = pd.merge(transactions, merchants, how='left')
    nrow_end = transactions.shape[0]
    if not PROTOTYPING:
        assert nrow_begin == nrow_end

    transactions['purchase_amount_raw'] = transactions['purchase_amount'] / 0.00150265118 + 497.06

    # fillna
    # transactions['category_2'].fillna(1.0, inplace=True)
    # transactions['category_3'].fillna('A', inplace=True)
    # transactions['merchant_id'].fillna('M_ID_00a6ca8a8a', inplace=True)
    transactions['installments'].replace(-1, np.nan, inplace=True)
    transactions['installments'].replace(999, np.nan, inplace=True)

    transactions['month_diff'] = ((datetime.datetime.today() - transactions['purchase_date']).dt.days)//30
    transactions['month_diff'] += transactions['month_lag']

    # Center all purchase date to mid of the month
    # Then move from month_lag
    # Then go to monthstart, this is the reference date from which loyalty socres are computed
    transactions['reference_date'] = transactions['purchase_date'] + pd.to_timedelta(15 - transactions['purchase_date'].dt.day, unit='d')
    transactions['reference_date'] = transactions['reference_date'] - transactions['month_lag'].values.astype("timedelta64[M]")
    transactions['reference_date'] = transactions['reference_date'] + pd.offsets.MonthBegin(0)
    transactions['reference_date'] = pd.to_datetime(transactions['reference_date'].dt.date)
    # assert (transactions.groupby('card_id')['reference_date'].nunique() == 1).all()
    transactions['days_diff'] = (transactions['reference_date'] - pd.to_datetime(transactions['purchase_date'].dt.date)).dt.days

    # https://www.kaggle.com/denzo123/a-closer-look-at-date-variabless
    transactions['hour'] = transactions.purchase_date.dt.hour
    # Tuesday as first day instead of Monday
    transactions['dow'] = (transactions.purchase_date.dt.dayofweek - 1).mod(7)
    transactions['automatic'] = ((transactions['hour'] + transactions.purchase_date.dt.hour) == 0).astype(int)
    transactions['nightclub'] = ((transactions['hour'] > 0) & (transactions['hour'] < 7)).astype(int)

    transactions = transactions
    # additional features
    transactions['price'] = transactions['purchase_amount'] / transactions['installments']

    # Christmas : December 25 2017
    transactions['Christmas_Day_2017'] = (pd.to_datetime('2017-12-25')-transactions['purchase_date']).dt.days.apply(lambda x: x if x > 0 and x < 100 else 0)
    # Mothers Day: May 14 2017
    transactions['Mothers_Day_2017'] = (pd.to_datetime('2017-06-04')-transactions['purchase_date']).dt.days.apply(lambda x: x if x > 0 and x < 100 else 0)
    # fathers day: August 13 2017
    transactions['fathers_day_2017'] = (pd.to_datetime('2017-08-13')-transactions['purchase_date']).dt.days.apply(lambda x: x if x > 0 and x < 100 else 0)
    # Childrens day: October 12 2017
    transactions['Children_day_2017'] = (pd.to_datetime('2017-10-12')-transactions['purchase_date']).dt.days.apply(lambda x: x if x > 0 and x < 100 else 0)
    # Valentine's Day : 12th June, 2017
    transactions['Valentine_Day_2017'] = (pd.to_datetime('2017-06-12')-transactions['purchase_date']).dt.days.apply(lambda x: x if x > 0 and x < 100 else 0)
    # Black Friday : 24th November 2017
    transactions['Black_Friday_2017'] = (pd.to_datetime('2017-11-24') - transactions['purchase_date']).dt.days.apply(lambda x: x if x > 0 and x < 100 else 0)

    # 2018
    # Mothers Day: May 13 2018
    transactions['Mothers_Day_2018'] = (pd.to_datetime('2018-05-13')-transactions['purchase_date']).dt.days.apply(lambda x: x if x > 0 and x < 100 else 0)

    """
    https://www.kaggle.com/tapioca/category-1-2-3-in-transactions
    Category 3 is based on how the payment is made (installments)
    Category 2 is based on the state_id.
    Category 1 is influenced by the city_id : If 1, means city_id is missing
    """

    for col in ['authorized_flag', 'category_1']:
        transactions[col] = transactions[col].map({'Y': 1, 'N': 0})

    # For production purpose, should first fit columns to category type, and save possible values
    keep_cols = transactions[['category_2', 'category_3', 'category_4']].copy()

    transactions = pd.get_dummies(transactions, columns=['category_2', 'category_3', 'category_4'], dummy_na=True)

    for col in keep_cols:
        transactions[col] = keep_cols[col]

    transactions = utils.reduce_mem_usage(transactions)

    return transactions
