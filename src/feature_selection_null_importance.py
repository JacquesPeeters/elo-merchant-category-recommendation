import pandas as pd
import numpy as np

# from tqdm import tqdm
from scipy.sparse import hstack
import pickle

# from sklearn.model_selection import StratifiedKFold
# from sklearn.metrics import mean_squared_error

# import ipdb
# import src.utils as utils
# import src.preprocessing as preprocessing
import src.feature_engineering as feature_engineering
import src.feature_selection as feature_selection

pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 8)

PROTOTYPING = False

param_regression = {
    'objective': 'regression_l2',
    'boosting_type': 'rf',
    'subsample': 0.623,
    'colsample_bytree': 0.7,
    'num_leaves': 127,
    'max_depth': 8,
    'bagging_freq': 1,
}

param_classif = {
    'objective': 'binary',
    'boosting_type': 'rf',
    'subsample': 0.623,
    'colsample_bytree': 0.7,
    'num_leaves': 127,
    'max_depth': 8,
    'bagging_freq': 1,
}

lgb_wo_outliers = {
    'target': 'target',
    'param': param_regression,
    'drop_outliers': True}

lgb_outliers = {
    'target': 'outliers',
    'param': param_classif,
    'drop_outliers': False}

lgb_outliers_regression = {
    'target': 'outliers',
    'param': param_regression,
    'drop_outliers': False}

lgb_single = {
    'target': 'target',
    'param': param_regression,
    'drop_outliers': False,
    }

dict_model = {}
dict_model['wo_outliers'] = lgb_wo_outliers
dict_model['outliers'] = lgb_outliers
dict_model['outliers_regression'] = lgb_outliers_regression
dict_model['single'] = lgb_single

df_learning, df_test, X_learning_sparse, X_test_sparse, X_cols_sparse = feature_engineering.prepare_data(PROTOTYPING)

to_drop = ['first_active_month', 'card_id', 'date_from', 'outliers', 'reference_date', 'target']

for model_name, model_config in dict_model.items():
    print(model_name)
    print('Feature selection for dict_model: {}'.format(model_name))
    target = model_config['target']
    X_learning_dense = df_learning.drop(to_drop + [target], axis=1, errors='ignore')
    y_learning = df_learning[target]
    is_outlier_learning = df_learning['outliers']

    X_cols = list(X_learning_dense)
    X_test_dense = df_test[X_cols]

    X_learning = hstack([X_learning_dense, X_learning_sparse], format='csr')
    X_test = hstack([X_test_dense, X_test_sparse], format='csr')

    X_colnames = list(X_learning_dense.columns) + X_cols_sparse

    param = model_config['param']

    # Remove outliers
    if model_config['drop_outliers']:
        learning_wo = np.where(is_outlier_learning != 1)[0]
        X_learning = X_learning[learning_wo]
        y_learning = y_learning.iloc[learning_wo]

    null_features = feature_selection.detect_null_features(X_learning, y_learning, X_colnames, param=param)

    with open('feature_store/{}_null_features.pickle'.format(model_name), 'wb') as file:
        pickle.dump(null_features, file, protocol=pickle.HIGHEST_PROTOCOL)
