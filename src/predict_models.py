import lightgbm as lgb

import pandas as pd
import numpy as np

from scipy.sparse import hstack
import pickle

from sklearn.model_selection import KFold

import src.utils as utils
import src.feature_engineering as feature_engineering

pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 8)

PROTOTYPING = False
use_feature_selection = True
drop_correlated_features = True
drop_sparse_merchant_id = False
bayes_param = True


def get_param(**kwargs):
    param = dict()
    for key, value in kwargs.items():
        key_tmp = key
        value_tmp = value
        if '_power2' in key_tmp:
            key_tmp = key_tmp.replace('_power2', '')
            value_tmp = 2**int(round(value_tmp))
        param[key_tmp] = round(value_tmp, 2)

        if key_tmp in ['max_depth', 'num_leaves', 'min_data_in_leaf']:
            param[key_tmp] = int(round(param[key_tmp]))

    return param


def override_params(param, bayes_param):
    bayes_param = get_param(**bayes_param)
    for key, value in bayes_param.items():
        param[key] = bayes_param[key]

    return param


learning_rate = 0.001

param_regression = {
    'objective': 'regression_l2',
    "metric": 'rmse',
    'learning_rate': learning_rate,
    'random_state': 1,
    "verbosity": -1,
}

param_classif = {
    'objective': 'binary',
    "metric": 'binary_logloss',
    'learning_rate': learning_rate,
    'random_state': 1,
    "verbosity": -1,
}

lgb_wo_outliers = {
    'target': 'target',
    'param': param_regression,
    'drop_outliers': True,
    'fun_metric': utils.rmse,
    }

lgb_outliers = {
    'target': 'outliers',
    'param': param_classif,
    'drop_outliers': False,
    'fun_metric': utils.logloss,
    }

lgb_outliers_regression = {
    'target': 'outliers',
    'param': param_regression,
    'drop_outliers': False,
    'fun_metric': utils.logloss,
    }

lgb_single = {
    'target': 'target',
    'param': param_regression,
    'drop_outliers': False,
    'fun_metric': utils.rmse,
    }

dict_model = {}
dict_model['wo_outliers'] = lgb_wo_outliers
dict_model['outliers'] = lgb_outliers
dict_model['outliers_regression'] = lgb_outliers_regression
dict_model['single'] = lgb_single

df_learning, df_test, X_learning_sparse, X_test_sparse, X_cols_sparse = feature_engineering.prepare_data(PROTOTYPING)

to_drop = ['first_active_month', 'card_id', 'date_from', 'outliers', 'reference_date', 'target']

for model_name, model_config in dict_model.items():
    print('Predict model_name: {}'.format(model_name))
    target = model_config['target']
    fun_metric = model_config['fun_metric']
    param = model_config['param']

    if bayes_param:
        with open('feature_store/{}_lgb_bayesopt.pickle'.format(model_name), 'rb') as file:
            lgb_bayesopt = pickle.load(file)

        param = override_params(param, lgb_bayesopt['params'])
        print(param)

    X_learning_dense = df_learning.drop(to_drop + [target], axis=1, errors='ignore')
    y_learning = df_learning[target]
    is_outlier_learning = df_learning['outliers']

    X_cols = list(X_learning_dense)
    X_test_dense = df_test[X_cols]

    X_learning = hstack([X_learning_dense, X_learning_sparse], format='csr')
    X_test = hstack([X_test_dense, X_test_sparse], format='csr')

    X_colnames = list(X_learning_dense) + X_cols_sparse

    X_learning, X_test, X_colnames = utils.select_features(
        X_learning=X_learning,
        X_test=X_test,
        model_name=model_name,
        X_colnames=X_colnames,
        use_feature_selection=use_feature_selection,
        drop_correlated_features=drop_correlated_features,
        drop_sparse_merchant_id=drop_sparse_merchant_id,
        )

    # folds = StratifiedKFold(n_splits=5, shuffle=True, random_state=1)
    folds = KFold(n_splits=6, shuffle=False)

    predict_valid = np.zeros(X_learning.shape[0])
    predict_train = np.zeros(X_learning.shape[0])
    df_test['target'] = 0

    df_metric = pd.DataFrame()

    for fold_, (trn_idx, val_idx) in enumerate(folds.split(X_learning)):
        print("fold n°{}".format(fold_))

        X_train = X_learning[trn_idx]
        X_valid = X_learning[val_idx]
        y_train = y_learning.iloc[trn_idx]
        y_valid = y_learning.iloc[val_idx]
        is_outlier_train = is_outlier_learning.iloc[trn_idx]
        is_outlier_valid = is_outlier_learning.iloc[val_idx]

        if model_config['drop_outliers']:
            train_wo = np.where(is_outlier_train != 1)[0]
            valid_wo = np.where(is_outlier_valid != 1)[0]
            lgb_train = lgb.Dataset(X_train[train_wo], label=y_train.iloc[train_wo])
            lgb_valid = lgb.Dataset(X_valid[valid_wo], label=y_valid.iloc[valid_wo])
        else:
            lgb_train = lgb.Dataset(X_train, label=y_train)
            lgb_valid = lgb.Dataset(X_valid, label=y_valid)

        model_gbm = lgb.train(param, lgb_train, 20000,
                              valid_sets=[lgb_train, lgb_valid],
                              early_stopping_rounds=300,
                              # categorical_feature=categorical_feature,
                              feature_name=X_colnames,
                              verbose_eval=200)

        df_metric_tmp = pd.DataFrame({'fold': [fold_],
                                      'training': [model_gbm.best_score['training'][param['metric']]],
                                      'valid': [model_gbm.best_score['valid_1'][param['metric']]],
                                      'best_iter': [model_gbm.best_iteration]})

        df_metric = pd.concat([df_metric, df_metric_tmp], sort=True)

        # Even if drop_outliers, predict them
        predict_train[trn_idx] += model_gbm.predict(X_train)
        predict_valid[val_idx] = model_gbm.predict(X_valid)
        df_test['target'] += model_gbm.predict(X_test) / folds.n_splits

        with open('feature_store/model_gbm_{}_fold{}.pickle'.format(model_name, fold_), 'wb') as file:
            pickle.dump(model_gbm, file, protocol=pickle.HIGHEST_PROTOCOL)

    train_score = df_metric['training'].mean().round(3)
    valid_score = df_metric['valid'].mean().round(3)

    print('train_score: {}'.format(train_score))
    print('valid_score: {}'.format(valid_score))

    df_predict = pd.DataFrame({
        'predict_{}'.format(model_name): predict_valid,
        'card_id': df_learning['card_id']})

    df_predict_test = pd.DataFrame({
        'predict_{}'.format(model_name): df_test['target'],
        'card_id': df_test['card_id']})

    df_predict = pd.concat([df_predict, df_predict_test], sort=True)

    df_predict.to_parquet('feature_store/df_predict_{}.parquet.gzip'.format(model_name), compression='gzip')
    utils.save_metric(df_metric, path='output/df_metric_{}.csv'.format(model_name))
