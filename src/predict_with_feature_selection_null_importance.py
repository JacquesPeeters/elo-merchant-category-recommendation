import lightgbm as lgb

import pandas as pd
import numpy as np

# from tqdm import tqdm
from scipy.sparse import hstack
import pickle

from sklearn.model_selection import StratifiedKFold, KFold
# from sklearn.model_selection import train_test_split

# import ipdb
import src.utils as utils
# import src.preprocessing as preprocessing
import src.feature_engineering as feature_engineering
import src.feature_selection as feature_selection

pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 8)

PROTOTYPING = False
use_feature_selection = True

param_regression = {
    'objective': 'regression_l2',
    "metric": 'rmse',
    'learning_rate': 0.005,
    'random_state': 1,
    'num_leaves': 50,
    # 'min_data_in_leaf': min_df,
    'max_depth': 10,
    # "feature_fraction": 0.9,
    "bagging_freq": 1,
    "bagging_fraction": 0.7,
    "lambda_l1": 0.1,
    "verbosity": -1,
}

param_classif = {
    'objective': 'binary',
    "metric": 'binary_logloss',
    'learning_rate': 0.005,
    'random_state': 1,
    'num_leaves': 50,
    # 'min_data_in_leaf': min_df,
    'max_depth': 10,
    # "feature_fraction": 0.9,
    "bagging_freq": 1,
    "bagging_fraction": 0.7,
    "lambda_l1": 0.1,
    "verbosity": -1,
}

lgb_wo_outliers = {
    'target': 'target',
    'param': param_regression,
    'drop_outliers': True,
    'fun_metric': utils.rmse,
    }

lgb_outliers = {
    'target': 'outliers',
    'param': param_classif,
    'drop_outliers': False,
    'fun_metric': utils.logloss,
    }

lgb_outliers_regression = {
    'target': 'outliers',
    'param': param_regression,
    'drop_outliers': False,
    'fun_metric': utils.logloss,
    }

lgb_single = {
    'target': 'target',
    'param': param_regression,
    'drop_outliers': False,
    'fun_metric': utils.rmse,
    }

dict_model = {}
dict_model['wo_outliers'] = lgb_wo_outliers
dict_model['outliers'] = lgb_outliers
dict_model['outliers_regression'] = lgb_outliers_regression
dict_model['single'] = lgb_single

df_learning, df_test, X_learning_sparse, X_test_sparse, X_cols_sparse = feature_engineering.prepare_data(PROTOTYPING)

to_drop = ['first_active_month', 'card_id', 'date_from', 'outliers', 'reference_date', 'target']

# model_config = lgb_classic
# model_name = 'classic'

for model_name, model_config in dict_model.items():
    print('Feature selection for dict_model: {}'.format(model_name))
    target = model_config['target']
    fun_metric = model_config['fun_metric']
    param = model_config['param']

    X_learning_dense = df_learning.drop(to_drop + [target], axis=1, errors='ignore')
    y_learning = df_learning[target]
    is_outlier_learning = df_learning['outliers']

    X_cols = list(X_learning_dense)
    X_test_dense = df_test[X_cols]

    X_learning = hstack([X_learning_dense, X_learning_sparse], format='csr')
    X_test = hstack([X_test_dense, X_test_sparse], format='csr')

    X_colnames = list(X_learning_dense) + X_cols_sparse

    if use_feature_selection:
        with open('feature_store/{}_null_features.pickle'.format(model_name), 'rb') as file:
            null_features = pickle.load(file)

        with open('feature_store/{}_correlated_features.pickle'.format(model_name), 'rb') as file:
            correlated_features = pickle.load(file)

        null_features = list(set(null_features + correlated_features))

        if model_name == 'wo_outliers':
            cols_refused = [i for i in X_colnames if 'refused_hist_' in i]
            null_features = list(set(null_features + cols_refused))

        cols_usefull_position = feature_selection.get_position_usefull_importance_features(X_colnames, null_features)
        usefull_features = [X_colnames[i] for i in cols_usefull_position]

        # Run one last time to see improvements
        X_colnames = usefull_features
        X_learning = X_learning[:, cols_usefull_position]
        X_test = X_test[:, cols_usefull_position]

    # folds = StratifiedKFold(n_splits=5, shuffle=True, random_state=1)
    folds = KFold(n_splits=6, shuffle=False)

    predict_valid = np.zeros(X_learning.shape[0])
    predict_train = np.zeros(X_learning.shape[0])
    df_test['target'] = 0

    df_metric = pd.DataFrame()

    for fold_, (trn_idx, val_idx) in enumerate(folds.split(X_learning, df_learning['outliers'].values)):
        print("fold n°{}".format(fold_))

        X_train = X_learning[trn_idx]
        X_valid = X_learning[val_idx]
        y_train = y_learning.iloc[trn_idx]
        y_valid = y_learning.iloc[val_idx]
        is_outlier_train = is_outlier_learning.iloc[trn_idx]
        is_outlier_valid = is_outlier_learning.iloc[val_idx]

        folds_inner = StratifiedKFold(n_splits=2, shuffle=True, random_state=1)
        for fold_inner, (val_idx_1, val_idx_2) in enumerate(folds_inner.split(X_valid, is_outlier_valid.values)):
            print("fold_inner n°{}".format(fold_inner))

            if model_config['drop_outliers']:
                train_wo = np.where(is_outlier_train != 1)[0]
                valid_wo_1 = np.where(is_outlier_valid.iloc[val_idx_1] != 1)[0]
                valid_wo_2 = np.where(is_outlier_valid.iloc[val_idx_2] != 1)[0]
                lgb_train = lgb.Dataset(X_train[train_wo], label=y_train.iloc[train_wo])
                X_valid_1 = X_valid[val_idx_1][valid_wo_1]
                y_valid_1 = y_valid.iloc[val_idx_1].iloc[valid_wo_1]
                X_valid_2 = X_valid[val_idx_2][valid_wo_2]
                y_valid_2 = y_valid.iloc[val_idx_2].iloc[valid_wo_2]
            else:
                lgb_train = lgb.Dataset(X_train, label=y_train)
                X_valid_1 = X_valid[val_idx_1]
                y_valid_1 = y_valid.iloc[val_idx_1]
                X_valid_2 = X_valid[val_idx_2]
                y_valid_2 = y_valid.iloc[val_idx_2]

            lgb_valid = lgb.Dataset(X_valid_1, label=y_valid_1)
            param['random_state'] = fold_inner

            model_gbm = lgb.train(param, lgb_train, 10000,  # 2000 iterations otherwise it still improves with 4000 iterations...
                                  valid_sets=[lgb_train, lgb_valid],
                                  early_stopping_rounds=300,
                                  # categorical_feature=categorical_feature,
                                  feature_name=X_colnames,
                                  verbose_eval=200)

            df_metric_tmp = pd.DataFrame({'fold': [fold_],
                                          'fold_inner': [fold_inner],
                                          'training': [model_gbm.best_score['training'][param['metric']]],
                                          'valid': [model_gbm.best_score['valid_1'][param['metric']]],
                                          'valid_2': [fun_metric(y_valid_2, model_gbm.predict(X_valid_2))],
                                          'best_iter': [model_gbm.best_iteration]})

            df_metric = pd.concat([df_metric, df_metric_tmp], sort=True)

            # Even if drop_outliers, predict them
            predict_train[trn_idx] += model_gbm.predict(X_train) / folds_inner.n_splits
            predict_valid[val_idx[val_idx_2]] = model_gbm.predict(X_valid[val_idx_2])
            df_test['target'] += model_gbm.predict(X_test) / folds.n_splits / folds_inner.n_splits

            with open('feature_store/model_gbm_{}_fold{}_{}.pickle'.format(model_name, fold_, fold_inner), 'wb') as file:
                pickle.dump(model_gbm, file, protocol=pickle.HIGHEST_PROTOCOL)

    df_predict = pd.DataFrame({
        'predict_{}'.format(model_name): predict_valid,
        'card_id': df_learning['card_id']})

    df_predict_test = pd.DataFrame({
        'predict_{}'.format(model_name): df_test['target'],
        'card_id': df_test['card_id']})

    df_predict = pd.concat([df_predict, df_predict_test], sort=True)

    df_predict.to_parquet('feature_store/df_predict_{}.parquet.gzip'.format(model_name), compression='gzip')
    utils.save_metric(df_metric, path='output/df_metric_{}.csv'.format(model_name))
