import pandas as pd
import numpy as np
# import lightgbm as lgb
# import sklearn as sk
import gc
# from scipy.sparse import hstack
import pickle

# from sklearn.model_selection import StratifiedKFold
# from sklearn.metrics import mean_squared_error

# import ipdb
import src.utils as utils # noqa
import src.preprocessing as preprocessing # noqa
import src.feature_engineering as feature_engineering

pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 8)

PROTOTYPING = False

# Load data --------------------------------------------------------------------
merchants = pd.read_csv('data/merchants.csv')
sample_submission = pd.read_csv('data/sample_submission.csv')
test = pd.read_csv('data/test.csv', parse_dates=["first_active_month"])
train = pd.read_csv('data/train.csv', parse_dates=["first_active_month"])

hist_trans = pd.read_csv('data/historical_transactions.csv', parse_dates=['purchase_date'])
hist_trans = utils.reduce_mem_usage(hist_trans)
new_trans = pd.read_csv('data/new_merchant_transactions.csv', parse_dates=['purchase_date'])
new_trans = utils.reduce_mem_usage(new_trans)

# Preprocessing ----------------------------------------------------------------
merchants = preprocessing.preprocessing_merchants(merchants)

if PROTOTYPING:
    # Keep all transactions from card_id finishing with 'd' : 1/16 sampling
    hist_trans = hist_trans[hist_trans['card_id'].str[-1] == 'd'].copy()
    new_trans = new_trans[new_trans['card_id'].str[-1] == 'd'].copy()

gc.collect()

hist_trans = preprocessing.preprocessing_transactions(hist_trans, merchants)
new_trans = preprocessing.preprocessing_transactions(new_trans, merchants)

auth_hist_trans = hist_trans[hist_trans['authorized_flag'] == 1]
refused_hist_trans = hist_trans[hist_trans['authorized_flag'] == 0]

# Feature engineering ----------------------------------------------------------
print('authorized_flag')
authorized_flag = utils.apply_func_before_date_from(hist_trans,
                                                    feature_engineering.fe_card_auth,
                                                    window_month=[None])

print('fe_repurchase')
fe_repurchase = feature_engineering.get_fe_repurchase(auth_hist_trans)

print('fe_reference_date')
train, test = feature_engineering.add_reference_date(hist_trans, train, test)
fe_reference_date = feature_engineering.fe_reference_date(train, test)


print('new')
new = utils.apply_func_before_date_from(new_trans,
                                        feature_engineering.fe_transactions_card,
                                        window_month=[None])
new = new.rename(lambda col:
                 utils.rename_add(col=col,
                                  str_prepend='new_',
                                  excepted=['date_from', 'card_id']),
                 axis=1)


print('hist')
hist = utils.apply_func_before_date_from(hist_trans,
                                         feature_engineering.fe_transactions_card,
                                         window_month=[None])
hist = new.rename(lambda col:
                  utils.rename_add(col=col,
                                   str_prepend='new_',
                                   excepted=['date_from', 'card_id']),
                  axis=1)

print('auth_hist')
auth_hist = utils.apply_func_before_date_from(auth_hist_trans,
                                              feature_engineering.fe_transactions_card,
                                              window_month=[None])
auth_hist = auth_hist.rename(lambda col:
                             utils.rename_add(col=col,
                                              str_prepend='auth_hist_',
                                              excepted=['date_from', 'card_id']),
                             axis=1)

print('refused_hist')
refused_hist = utils.apply_func_before_date_from(refused_hist_trans,
                                                 feature_engineering.fe_transactions_card,
                                                 window_month=[None])
refused_hist = refused_hist.rename(lambda col:
                                   utils.rename_add(col=col,
                                                    str_prepend='refused_hist_',
                                                    excepted=['date_from', 'card_id']),
                                   axis=1)

print('df_countvectorize')
# print('A challenger')
min_df = 20
count_vectorizer, df_countvectorize = feature_engineering.get_count_vectorizer(auth_hist_trans,
                                                                               train,
                                                                               min_df=min_df)

count_vectorizer_hist, df_countvectorize_hist = feature_engineering.get_count_vectorizer(hist_trans,
                                                                                         train,
                                                                                         min_df=min_df)


if not PROTOTYPING:
    print('Save to featuresets')
    authorized_flag.to_parquet('feature_store/authorized_flag.parquet.gzip', compression='gzip')
    fe_repurchase.to_parquet('feature_store/fe_repurchase.parquet.gzip', compression='gzip')
    new.to_parquet('feature_store/new.parquet.gzip', compression='gzip')
    hist.to_parquet('feature_store/hist.parquet.gzip', compression='gzip')
    auth_hist.to_parquet('feature_store/auth_hist.parquet.gzip', compression='gzip')
    refused_hist.to_parquet('feature_store/refused_hist.parquet.gzip', compression='gzip')
    fe_reference_date.to_parquet('feature_store/fe_reference_date.parquet.gzip', compression='gzip')

    df_countvectorize.to_parquet('feature_store/df_countvectorize.parquet.gzip', compression='gzip')
    with open('feature_store/count_vectorizer.pickle', 'wb') as file:
        pickle.dump(count_vectorizer, file, protocol=pickle.HIGHEST_PROTOCOL)

    df_countvectorize_hist.to_parquet('feature_store/df_countvectorize_hist.parquet.gzip', compression='gzip')
    with open('feature_store/count_vectorizer_hist.pickle', 'wb') as file:
        pickle.dump(count_vectorizer_hist, file, protocol=pickle.HIGHEST_PROTOCOL)

    with open('feature_store/min_df.pickle', 'wb') as file:
        pickle.dump(min_df, file, protocol=pickle.HIGHEST_PROTOCOL)

    print('End of create_featuresets.py')
