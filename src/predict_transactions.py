import pandas as pd
import numpy as np

import gc

from sklearn.model_selection import KFold

import keras
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from sklearn.model_selection import train_test_split

from keras import backend as K
K.tensorflow_backend._get_available_gpus()

# import ipdb
import src.utils as utils # noqa
import src.preprocessing as preprocessing # noqa
import src.feature_engineering as feature_engineering

pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 8)

PROTOTYPING = True
model_name = 'lstm'


def get_X_y(df_learning, to_drop, target, is_train):
    groups = df_learning.groupby(['card_id'])
    list_cardid = []
    list_X = []
    list_y = []
    for keys, group in groups:
        X = group.drop(columns=to_drop, errors='ignore').values
        list_X.append(X)
        list_cardid.append(keys)
        if is_train:
            y = group[target].drop_duplicates().values[0]
            list_y.append(y)
    if is_train:
        res = (list_cardid, list_X, list_y)
    else:
        res = (list_cardid, list_X)

    return res


def get_lstm_model(input_shape):
    model = Sequential()
    model.add(LSTM(256, input_shape=input_shape))
    # model.add(Dense(1, activation='sigmoid'))
    # model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.add(Dense(2, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam')
    return model


merchants = pd.read_csv('data/merchants.csv')
sample_submission = pd.read_csv('data/sample_submission.csv')
test = pd.read_csv('data/test.csv', parse_dates=["first_active_month"])
train = pd.read_csv('data/train.csv', parse_dates=["first_active_month"])

hist_trans = pd.read_csv('data/historical_transactions.csv', parse_dates=['purchase_date'])
hist_trans = utils.reduce_mem_usage(hist_trans)
new_trans = pd.read_csv('data/new_merchant_transactions.csv', parse_dates=['purchase_date'])
new_trans = utils.reduce_mem_usage(new_trans)

if PROTOTYPING:
    # Keep all transactions from card_id finishing with 'd' : 1/16 sampling
    hist_trans = hist_trans[hist_trans['card_id'].str[-1] == 'd'].copy()
    new_trans = new_trans[new_trans['card_id'].str[-1] == 'd'].copy()

gc.collect()
print('Corriger ici')
hist_trans = preprocessing.preprocessing_transactions(hist_trans, merchants[['merchant_id', 'category_4']], PROTOTYPING)
new_trans = preprocessing.preprocessing_transactions(new_trans, merchants[['merchant_id', 'category_4']], PROTOTYPING)

transactions = pd.concat([hist_trans.eval('is_new = 0'),
                          new_trans.eval('is_new = 1')], sort=True)

transactions = transactions.reset_index(drop=True)
transactions = pd.merge(transactions, pd.concat([train, test], sort=True), how='left')

transactions['outliers'] = 0
transactions.loc[transactions['target'] < -30, 'outliers'] = 1
transactions.loc[transactions['target'].isna(), 'is_outlier'] = np.nan
transactions['outliers'].describe()

# cols_to_le = ['merchant_id', 'category_3']
# dict_le = {}
# for col in cols_to_le:
#     transactions_target[col] = transactions_target[col].fillna('NA')
#     le = LabelEncoder()
#     le.fit(transactions_target[col].fillna('NA'))
#     dict_le[col] = le
#     transactions_target[col] = le.transform(transactions_target[col].fillna('NA'))


# # Assign folders to card_id
# card_fold = transactions[['card_id']].drop_duplicates()
# N_FOLD = 10
# np.random.seed(0)
# card_fold['fold'] = np.random.randint(N_FOLD, size=card_fold.shape[0])
# transactions = pd.merge(card_fold, transactions)


# print('Target embedding :')
# # TODO ici FE oof
# # cols_to_embed = ['merchant_id', 'city_id']
# # col_groups = list(combinations(cols_to_embed, 2))
# # col_groups = [list(g) for g in col_groups]
# col_groups = [['city_id'],
#               ['city_id', 'merchant_id'],
#               ['merchant_category_id']]
#
# # [['city_id', 'merchant_id', 'month']]
# for col_group in col_groups:
#     print(col_group)
#     df_embed = utils.oof_dataframe(data=transactions.drop_duplicates(['card_id', 'merchant_id']),
#                                    col_fold='fold',
#                                    function=utils.target_embedding,
#                                    col=col_group,
#                                    target='is_outlier'
#                                    )
#     transactions = pd.merge(transactions, df_embed, how='left')


# print('Frequency encoding')
# col_groups = [['city_id', 'merchant_id', 'month']]
# for col_group in col_groups:
#     new_col = '_'.join(col_group)
#     transactions_target.groupby('merchant_id').agg({'diff': ['size']}).shape
#     df_frequency = transactions_target.groupby(col_group).agg({'diff': ['size']})
#     df_frequency.columns = [new_col + '_' + '_'.join(col).strip() for col in df_frequency.columns]
#     df_frequency = df_frequency.reset_index()
#     transactions_target = pd.merge(transactions_target, df_frequency, how='left')


target = 'outliers'
df_learning = pd.merge(train[['card_id']], transactions)
df_test = pd.merge(test[['card_id']], transactions)

to_drop = ['card_id', 'purchase_date', 'transaction_id', 'fold', 'merchant_id',
           'reference_date', 'category_3', 'first_active_month',
           'category_4']

cardid_learning, X_learning, y = get_X_y(df_learning, to_drop, target, is_train=True)
y = keras.utils.to_categorical(y, num_classes=None, dtype='float32')
cardid_test, X_test = get_X_y(df_test, to_drop, target, is_train=False)

group_size = df_learning.groupby(['card_id']).size().max()
X_learning = pad_sequences(X_learning, maxlen=group_size)
X_test = pad_sequences(X_test, maxlen=group_size)

predict_valid = np.zeros(len(X_learning))
predict_train = np.zeros(len(X_learning))

df_metric = pd.DataFrame()
folds = KFold(n_splits=5, shuffle=True, random_state=1)



# X_train, X_test, y_train, y_test = train_test_split(X_learning, y, test_size=0.33, random_state=1)
# model = Sequential()
# model.add(LSTM(256, input_shape=X_train[0].shape))
# # model.add(Dense(1, activation='sigmoid'))
# # model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
# model.add(Dense(2, activation='softmax'))
# model.compile(loss='categorical_crossentropy', optimizer='adam')
# print(model.summary())
# model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=1, batch_size=1024)


for fold_, (trn_idx, val_idx) in enumerate(folds.split(X_learning)):
    print("fold n°{}".format(fold_))
    X_train = X_learning[trn_idx, :, :]
    X_valid = X_learning[val_idx, :, :]
    y_train = y[trn_idx, :]
    y_valid = y[val_idx, :]

    model = get_lstm_model(input_shape=X_train[0].shape)
    model.fit(X_train, y_train,
              batch_size=64,
              epochs=1,
              validation_data=(X_valid, y_valid))

    pred_train = model.predict(X_train)
    pred_valid = model.predict(X_valid)
    pred_test = model.predict(X_test)

    train_score = round(utils.log_loss(y_true=y_train[:, 1], y_pred=pred_train[:, 1]), 3)
    valid_score = round(utils.log_loss(y_true=y_valid[:, 1], y_pred=pred_valid[:, 1]), 3)
    print('train_score: {}'.format(train_score))
    print('valid_score: {}'.format(valid_score))

    predict_train[trn_idx] += pred_train[:, 1]
    predict_valid[val_idx] = pred_valid[:, 1]
    df_test['target'] += pred_test / folds.n_splits

    df_metric_tmp = pd.DataFrame({'fold': [fold_],
                                  'training': [train_score],
                                  'valid': [valid_score],
                                  })

df_predict = pd.DataFrame({
    'predict_{}'.format(model_name): predict_valid,
    'card_id': cardid_learning})

df_predict_test = pd.DataFrame({
    'predict_{}'.format(model_name): df_test['target'],
    'card_id': cardid_test})

df_predict = pd.concat([df_predict, df_predict_test], sort=True)
df_metric = pd.concat([df_metric, df_metric_tmp], sort=True)

fname_df_predict = 'feature_store/machine/df_predict_{}.parquet.gzip'.format(model_name)
fname_df_metric = 'output/machine/df_metric_{}.csv'.format(model_name)
df_predict.to_parquet(fname_df_predict, compression='gzip')
utils.save_metric(df_metric, path=fname_df_metric)
