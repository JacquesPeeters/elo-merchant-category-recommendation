import lightgbm as lgb

import pandas as pd
import numpy as np

# from tqdm import tqdm
from scipy.sparse import hstack
import pickle

# from sklearn.model_selection import StratifiedKFold
# from sklearn.metrics import mean_squared_error

from bayes_opt import BayesianOptimization
from sklearn.model_selection import train_test_split

# import ipdb
import src.utils as utils
# import src.preprocessing as preprocessing
import src.feature_engineering as feature_engineering

pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 8)

PROTOTYPING = False
use_feature_selection = True
drop_correlated_features = False
drop_sparse_merchant_id = False

learning_rate = 0.005

param_regression = {
    'objective': 'regression_l2',
    "metric": 'rmse',
    'learning_rate': learning_rate,
    'random_state': 1,
    "verbosity": -1,
}

param_classif = {
    'objective': 'binary',
    "metric": 'binary_logloss',
    'learning_rate': learning_rate,
    'random_state': 1,
    "verbosity": -1,
}

lgb_wo_outliers = {
    'target': 'target',
    'param': param_regression,
    'drop_outliers': True,
    'fun_metric': utils.rmse,
    }

lgb_outliers = {
    'target': 'outliers',
    'param': param_classif,
    'drop_outliers': False,
    'fun_metric': utils.logloss,
    }

lgb_outliers_regression = {
    'target': 'outliers',
    'param': param_regression,
    'drop_outliers': False,
    'fun_metric': utils.logloss,
    }

lgb_single = {
    'target': 'target',
    'param': param_regression,
    'drop_outliers': False,
    'fun_metric': utils.rmse,
    }

dict_model = {}
dict_model['wo_outliers'] = lgb_wo_outliers
dict_model['outliers'] = lgb_outliers
dict_model['outliers_regression'] = lgb_outliers_regression
dict_model['single'] = lgb_single


def lgb_score(X_learning, y_learning, X_colnames, is_outlier_learning, base_param):
    X_train, X_valid, y_train, y_valid = train_test_split(X_learning, y_learning,
                                                          test_size=0.2,
                                                          random_state=1,
                                                          stratify=is_outlier_learning)

    lgb_train = lgb.Dataset(X_train, label=y_train, free_raw_data=False, silent=True)
    lgb_valid = lgb.Dataset(X_valid, label=y_valid, free_raw_data=False, silent=True)

    def tmp_lgb_score(**kwargs):
        def get_param(**kwargs):
            param = dict()
            for key, value in kwargs.items():
                key_tmp = key
                value_tmp = value
                if '_power2' in key_tmp:
                    key_tmp = key_tmp.replace('_power2', '')
                    value_tmp = 2**int(round(value_tmp))
                param[key_tmp] = value_tmp

                if key_tmp in ['max_depth', 'num_leaves', 'min_data_in_leaf']:
                    param[key_tmp] = int(round(param[key_tmp]))

            return param

        param = get_param(**kwargs)

        param['metric'] = base_param['metric']
        param['learning_rate'] = 0.01
        param['bagging_freq'] = 1

        # Fit the model
        model_gbm = lgb.train(params=param,
                              train_set=lgb_train,
                              num_boost_round=20000,
                              valid_sets=[lgb_train, lgb_valid],
                              early_stopping_rounds=300,
                              # categorical_feature=categorical_feature,
                              feature_name=X_colnames,
                              verbose_eval=False)
        return -model_gbm.best_score['valid_1'][param['metric']]

    print('Base score:')
    print(tmp_lgb_score(**base_param))
    print('\n')

    lgb_bayesopt = BayesianOptimization(f=tmp_lgb_score,
                                        pbounds={
                                            'num_leaves_power2': (4, 12),
                                            'min_data_in_leaf_power2': (4, 12),
                                            'feature_fraction': (0.1, 1.0),
                                            'bagging_fraction': (0.1, 1.0),
                                            'lambda_l1_power2': (-8, 5),
                                            'lambda_l2_power2': (-8, 5),
                                            'min_gain_to_split_power2': (-8, 5),
                                            },
                                        random_state=1,
                                        )

    lgb_bayesopt.maximize(init_points=5, n_iter=50)
    print("Final result:", lgb_bayesopt.max)

    return lgb_bayesopt


df_learning, df_test, X_learning_sparse, X_test_sparse, X_cols_sparse = feature_engineering.prepare_data(PROTOTYPING)

to_drop = ['first_active_month', 'card_id', 'date_from', 'outliers', 'reference_date', 'target']

for model_name, model_config in dict_model.items():
    print('Feature selection for dict_model: {}'.format(model_name))
    target = model_config['target']
    fun_metric = model_config['fun_metric']
    param = model_config['param']

    X_learning_dense = df_learning.drop(to_drop + [target], axis=1, errors='ignore')
    y_learning = df_learning[target]
    is_outlier_learning = df_learning['outliers']

    X_cols = list(X_learning_dense)
    X_test_dense = df_test[X_cols]

    X_learning = hstack([X_learning_dense, X_learning_sparse], format='csr')
    X_test = hstack([X_test_dense, X_test_sparse], format='csr')

    X_colnames = X_cols + X_cols_sparse

    param = model_config['param']

    X_learning, X_test, X_colnames = utils.select_features(
        X_learning=X_learning,
        X_test=X_test,
        model_name=model_name,
        X_colnames=X_colnames,
        use_feature_selection=use_feature_selection,
        drop_correlated_features=drop_correlated_features,
        drop_sparse_merchant_id=drop_sparse_merchant_id,
        )

    # Remove outliers
    if model_config['drop_outliers']:
        learning_wo = np.where(is_outlier_learning != 1)[0]
        X_learning = X_learning[learning_wo]
        y_learning = y_learning.iloc[learning_wo]
        stratify_on = None
    else:
        stratify_on = is_outlier_learning

    lgb_bayesopt = lgb_score(X_learning, y_learning, X_colnames, stratify_on, base_param=param)
    with open('feature_store/{}_lgb_bayesopt.pickle'.format(model_name), 'wb') as file:
        pickle.dump(lgb_bayesopt.max, file, protocol=pickle.HIGHEST_PROTOCOL)
