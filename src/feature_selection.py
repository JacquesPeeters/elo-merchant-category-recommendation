import lightgbm as lgb

import pandas as pd
import numpy as np

from tqdm import tqdm
# from scipy.sparse import hstack
# import pickle

# from sklearn.model_selection import StratifiedKFold
# from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split

# import ipdb
import src.utils as utils # noqa
import src.preprocessing as preprocessing # noqa
# import src.feature_engineering as feature_engineering


def get_position_usefull_importance_features(X_colnames, X_colnames_null):
    cols_not_null_position_end = []
    for i, j in enumerate(X_colnames):
        if j not in (X_colnames_null):
            cols_not_null_position_end.append(i)

    return cols_not_null_position_end


def get_feature_importances(X_learning, y_learning, X_colnames, shuffle, param, seed=None):
    # Go over fold and keep track of CV score (train and valid) and feature importances

    # Shuffle target if required
    if shuffle:
        # Here you could as well use a binomial distribution
        y_learning = y_learning.sample(frac=1.0)

    param['seed'] = seed
    X_train, X_valid, y_train, y_valid = train_test_split(X_learning, y_learning,
                                                          test_size=0.2,
                                                          random_state=1)

    lgb_train = lgb.Dataset(X_train, label=y_train, free_raw_data=False, silent=True)
    lgb_valid = lgb.Dataset(X_valid, label=y_valid, free_raw_data=False, silent=True)

    # Fit the model
    model_gbm = lgb.train(params=param,
                          train_set=lgb_train,
                          num_boost_round=200,
                          valid_sets=[lgb_train, lgb_valid],
                          # categorical_feature=categorical_feature,
                          feature_name=X_colnames,
                          verbose_eval=False)

    if not shuffle:
        print('model_gbm.best_score')
        print(model_gbm.best_score)

    # Get feature importances
    imp_df = pd.DataFrame()
    imp_df["feature"] = list(X_colnames)
    imp_df["importance_gain"] = model_gbm.feature_importance(importance_type='gain')
    imp_df["importance_split"] = model_gbm.feature_importance(importance_type='split')

    return imp_df


def detect_null_features(X_learning, y_learning, X_colnames, param):
    # Seed the unexpected randomness of this world
    np.random.seed(123)
    # Get the actual importance, i.e. without shuffling
    actual_imp_df = get_feature_importances(X_learning=X_learning,
                                            y_learning=y_learning,
                                            X_colnames=X_colnames,
                                            param=param,
                                            shuffle=False)
    actual_imp_df = actual_imp_df.reset_index().rename(columns={'index': 'position'})

    # Pre-filter with 0 gain features
    cols_null = list(actual_imp_df[actual_imp_df['importance_gain'] == 0].feature)
    cols_not_null_position = get_position_usefull_importance_features(X_colnames, cols_null)

    X_colnames_tmp = [X_colnames[i] for i in cols_not_null_position]
    X_learning_tmp = X_learning[:, cols_not_null_position]

    null_imp_df = pd.DataFrame()
    nb_runs = 100

    for i in tqdm(range(nb_runs)):
        # Get current run importances
        imp_df = get_feature_importances(X_learning=X_learning_tmp,
                                         y_learning=y_learning,
                                         X_colnames=X_colnames_tmp,
                                         param=param,
                                         seed=i,
                                         shuffle=True)
        imp_df['run'] = i + 1
        # Concat the latest importances with the old ones
        null_imp_df = pd.concat([null_imp_df, imp_df], axis=0)

    actual_imp_df = actual_imp_df[actual_imp_df['importance_gain'] > 0].copy()
    full_imp = pd.concat([null_imp_df.eval('actual = 0'), actual_imp_df.eval('actual = 1')], sort=False)
    full_imp['importance_gain_rank'] = full_imp.groupby('feature')['importance_gain'].rank(ascending=False)

    # We consider features as null if actual importance is not ranked first
    # It might looks greedy, we would drop a not null features, but it would be a feature with low importance in global model
    cols_null_importance = full_imp[(full_imp['actual'] == 1) & (full_imp['importance_gain_rank'] != 1)].feature.tolist()

    null_features = cols_null + cols_null_importance
    cols_usefull_position = get_position_usefull_importance_features(X_colnames, null_features)

    usefull_features = [X_colnames[i] for i in cols_usefull_position]
    assert len(cols_usefull_position) == len(full_imp[(full_imp['actual'] == 1) & (full_imp['importance_gain_rank'] == 1)])
    print('{} null features detected.'.format(len(null_features)))
    print('{} usefull features detected.'.format(len(usefull_features)))

    # Run one last time to see improvements
    X_colnames_tmp = usefull_features
    X_learning_tmp = X_learning[:, cols_usefull_position]
    actual_imp_df = get_feature_importances(X_learning=X_learning_tmp,
                                            y_learning=y_learning,
                                            X_colnames=X_colnames_tmp,
                                            param=param,
                                            shuffle=False)

    return null_features
