import pandas as pd
# import numpy as np

# import lightgbm as lgb
# import sklearn as sk
# import gc

# from scipy.sparse import hstack
# import pickle

# from sklearn.model_selection import StratifiedKFold
from sklearn.linear_model import Ridge

# import ipdb
import src.utils as utils # noqa
import src.preprocessing as preprocessing # noqa
import src.feature_engineering as feature_engineering

pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 8)

PROTOTYPING = False

test = pd.read_csv('data/test.csv', parse_dates=["first_active_month"])
train = pd.read_csv('data/train.csv', parse_dates=["first_active_month"])

model_name = 'wo_outliers'
df_predict_wo_outliers = pd.read_parquet('feature_store/df_predict_{}.parquet.gzip'.format(model_name)).reset_index(drop=True)
model_name = 'outliers'
df_predict_outliers = pd.read_parquet('feature_store/df_predict_{}.parquet.gzip'.format(model_name)).reset_index(drop=True)
model_name = 'outliers_regression'
df_predict_outliers_regression = pd.read_parquet('feature_store/df_predict_{}.parquet.gzip'.format(model_name)).reset_index(drop=True)
model_name = 'single'
df_predict_single = pd.read_parquet('feature_store/df_predict_{}.parquet.gzip'.format(model_name)).reset_index(drop=True)

df_learning = feature_engineering.get_learning(data=train,
                                               feature_sets=[df_predict_outliers,
                                                             df_predict_wo_outliers,
                                                             df_predict_outliers_regression,
                                                             df_predict_single],
                                               PROTOTYPING=PROTOTYPING)
df_test = feature_engineering.get_learning(data=test,
                                           feature_sets=[df_predict_outliers,
                                                         df_predict_wo_outliers,
                                                         df_predict_outliers_regression,
                                                         df_predict_single],
                                           PROTOTYPING=PROTOTYPING)

# df_learning = feature_engineering.fe_learning(df_learning)
# df_test = feature_engineering.fe_learning(df_test)
df_learning = feature_engineering.fe_learning_submodels(df_learning)
df_test = feature_engineering.fe_learning_submodels(df_test)

target = 'target'
y_learning = df_learning[target]

df_learning['outliers'] = 0
df_learning.loc[df_learning['target'] < -30, 'outliers'] = 1

to_drop = ['first_active_month', 'card_id', 'date_from', 'outliers',
           'reference_date', 'target',
           'feature_1', 'feature_2', 'feature_3']

X_learning = df_learning.drop(to_drop + [target], axis=1, errors='ignore')
X_test = df_test.drop(to_drop + [target], axis=1, errors='ignore')
y_learning = df_learning[target]
is_outlier_learning = df_learning['outliers']

X_cols = list(X_learning)
X_colnames = X_cols
X_test = df_test[X_cols]

# If we want to do Ridge instead of hard rule
model_ridge = Ridge()
model_ridge.fit(X_learning, y_learning)
df_learning['predict_stacker_ridge'] = model_ridge.predict(X_learning)
df_test['predict_stacker_ridge'] = model_ridge.predict(X_test)
score_ridge = round(utils.rmse(y_learning, df_learning['predict_stacker_ridge']), 3)
print('score_ridge: {}'.format(score_ridge))

# (ggplot(df_learning.sample(50000), aes('predict_outliers', 'predict_outliers_regression')) +
#  geom_point()+
#  geom_abline(slope=1))
# df_learning['predict_outliers_regression'] = df_learning['predict_outliers_regression'].round(2)
# ggplot(df_learning.groupby('predict_outliers_regression')['outliers'].mean().reset_index(), aes('predict_outliers_regression', 'outliers')) + geom_point() + geom_abline(slope=1)

# col_submit = 'predict_outliers_stack_predict_wo_outliers_sum'
col_submit = 'predict_stacker_ridge'
df_test['target'] = df_test[col_submit]

local_score = round(utils.rmse(y_learning, df_learning[col_submit]), 3)
print("local_score: {}".format(local_score, 3))
df_test[['card_id', 'target']].to_csv('output/submission.csv', index=False)

cmd_submit_kaggle = 'kaggle competitions submit -c elo-merchant-category-recommendation -f output/submission.csv -m "{} col_submit : {}"  '.format(message, col_submit)
print(cmd_submit_kaggle)

# df_predict_test.rename(columns={'predict_single': 'target'})[['card_id', 'target']].to_csv('output/submission.csv', index=False)
