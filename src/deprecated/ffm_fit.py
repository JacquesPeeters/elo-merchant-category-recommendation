import xlearn as xl

fname_train = "train_ffm.txt"
fname_valid = "valid_ffm.txt"
fname_test = "all_ffm.txt"

ffm_model = xl.create_ffm()
ffm_model.setTrain(fname_train)
ffm_model.setValidate(fname_valid)
ffm_model.setTest(fname_valid)

param = {'task': 'reg', 'lr': 0.001,
         'lambda': 0.2, 'metric': 'rmse'}
ffm_model.fit(param, "model.out")
