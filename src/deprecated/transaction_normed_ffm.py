from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

import pandas as pd

import lightgbm as lgb
# import sklearn as sk
import gc

# from sklearn.model_selection import KFold
# from sklearn.metrics import mean_squared_error

# import ipdb
import src.utils as utils # noqa
import src.preprocessing as preprocessing # noqa
# import src.feature_engineering as feature_engineering

pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 8)

PROTOTYPING = True

merchants = pd.read_csv('data/merchants.csv')
sample_submission = pd.read_csv('data/sample_submission.csv')
test = pd.read_csv('data/test.csv', parse_dates=["first_active_month"])
train = pd.read_csv('data/train.csv', parse_dates=["first_active_month"])

historical_transactions = pd.read_csv('data/historical_transactions.csv', parse_dates=['purchase_date'])
historical_transactions = utils.reduce_mem_usage(historical_transactions)
new_transactions = pd.read_csv('data/new_merchant_transactions.csv', parse_dates=['purchase_date'])
new_transactions = utils.reduce_mem_usage(new_transactions)

train_date_from = historical_transactions.purchase_date.max().to_period('M').to_timestamp()
test_date_from = new_transactions.purchase_date.max().to_period('M').to_timestamp()
l_date_from = [train_date_from, test_date_from]

train['date_from'] = train_date_from
test['date_from'] = test_date_from

# Suppr cette colonne?
# historical_transactions.month_lag.describe()
# new_transactions.month_lag.describe()

if PROTOTYPING:
    # Keep all transactions from card_id finishing with 'fc'
    historical_transactions = historical_transactions[historical_transactions['card_id'].str[-1] == 'd'].copy()
    new_transactions = new_transactions[new_transactions['card_id'].str[-1] == 'd'].copy()
    # historical_transactions = historical_transactions.sample(frac=0.1)
    # new_transactions = new_transactions.sample(frac=0.1)

transactions = pd.concat([historical_transactions, new_transactions], axis=0)
del historical_transactions, new_transactions
gc.collect()
transactions = preprocessing.preprocessing_transactions(transactions)

# Y a des doublons potentiellement
# transactions.shape[0] - transactions.drop_duplicates(['card_id', 'purchase_date', 'merchant_id', 'authorized_flag', 'purchase_amount']).shape[0]

transactions_learning = pd.merge(transactions, train[['card_id', 'target']], how='left')

# IDEA : remove date trend from target in transaction
# THEN : apply FE
data = transactions_learning.groupby(['purchase_date_month'])['target'].mean().reset_index()
data['month'] = data['purchase_date_month'].dt.month
data['year'] = data['purchase_date_month'].dt.year

model_reg = LinearRegression()
model_reg.fit(data[['month', 'year']], data['target'])

data['pred'] = model_reg.predict(data[['month', 'year']])
data['diff'] = data['pred'] - data['target']
# ggplot(data, aes('purchase_date_month', 'pred')) + geom_line() + geom_line(aes(y='target'), color='red')
# ggplot(data, aes('purchase_date_month', 'diff')) + geom_line()

transactions_learning = pd.merge(transactions_learning, data[['purchase_date_month', 'pred']])
transactions_learning['diff'] = transactions_learning['target'] - transactions_learning['pred']
transactions_learning = transactions_learning.drop(columns=['target', 'pred'])

# transactions_learning = transactions_learning.filter(regex='^((?!category_2_).)*$', axis=1)
# transactions_learning = transactions_learning.filter(regex='^((?!category_3_).)*$', axis=1)
transactions_learning['month'] = transactions_learning['purchase_date_month'].dt.month
# transactions_learning = transactions_learning.drop(columns=['card_id', 'purchase_date', 'purchase_date_month'])

cols_to_le = ['merchant_id', 'category_3']
dict_le = {}
for col in cols_to_le:
    transactions_learning[col] = transactions_learning[col].fillna('NA')
    le = LabelEncoder()
    le.fit(transactions_learning[col].fillna('NA'))
    dict_le[col] = le
    transactions_learning[col] = le.transform(transactions_learning[col].fillna('NA'))

# cols_numerical = ['purchase_amount']
# for col in cols_numerical:
#     print(col)
#     transactions_learning[col] = pd.qcut(transactions_learning[col], 50, labels=False, retbins=False).nunique()

# fill NA for libFM's format
cols_fill = transactions_learning.columns != 'diff'
transactions_learning.loc[:, cols_fill] = transactions_learning.loc[:, cols_fill].fillna(-1)

# cols_categories = transactions_learning.drop(columns=cols_numerical).columns

is_train = transactions_learning['diff'].notnull()
transactions_target = transactions_learning[is_train]

transactions_train, transactions_valid = train_test_split(transactions_target, test_size=0.5)

to_drop = ['card_id', 'purchase_date', 'purchase_date_month']
X_train = transactions_train.drop(to_drop + ['diff'], axis=1)
y_train = transactions_train['diff']
X_valid = transactions_valid.drop(to_drop + ['diff'], axis=1)
y_valid = transactions_valid['diff']


print("Create lgb's dataset")
lgb_train = lgb.Dataset(X_train, label=y_train)
lgb_valid = lgb.Dataset(X_valid, label=y_valid)

print("Training model")
param = {'objective': 'regression_l2', 'metrics': 'rmse'}
model_gbm = lgb.train(param, lgb_train, 1000,
                      valid_sets=[lgb_train, lgb_valid],
                      early_stopping_rounds=100,
                      # categorical_feature=categorical_feature,
                      verbose_eval=100)

pred = model_gbm.predict(X_valid)
pd.Series(pred).describe()

transactions_target.drop(to_drop + ['diff'], axis=1)

# fname_train = "train_ffm.txt"
# utils.convert_to_ffm(df=transactions_train,
#                      target='diff',
#                      numerics=cols_numerical,
#                      categories=cols_categories,
#                      fname=fname_train)
#
# fname_valid = "valid_ffm.txt"
# utils.convert_to_ffm(df=transactions_test,
#                      target='diff',
#                      numerics=cols_numerical,
#                      categories=cols_categories,
#                      fname=fname_valid)
#
# fname_test = "all_ffm.txt"
# utils.convert_to_ffm(df=transactions_learning.fillna(-1),
#                      target='diff',
#                      numerics=cols_numerical,
#                      categories=cols_categories,
#                      fname=fname_test)
#
# import xlearn as xl
#
# ffm_model = xl.create_ffm()
# ffm_model.setTrain(fname_train)
# ffm_model.setValidate(fname_valid)
# ffm_model.setTest(fname_valid)
#
# param = {'task': 'reg', 'lr': 0.01,
#          'lambda': 0.002, 'metric': 'rmse'}
# ffm_model.fit(param, "model.out")
# # ffm_model.predict(fname_valid, 'predict.out')
# ffm_model.predict("./model.out", "./output.txt")
# pd.read_csv('output.txt', header=None)[0].values
# pd.read_csv('output.txt', header=None).describe()
# transactions_learning['diff'].describe()
