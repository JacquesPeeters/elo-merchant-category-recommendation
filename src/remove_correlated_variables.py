# import lightgbm as lgb

import pandas as pd
import numpy as np

# from tqdm import tqdm
# from scipy.sparse import hstack
import pickle
from itertools import chain

# from sklearn.model_selection import StratifiedKFold
# from sklearn.metrics import mean_squared_error


# import ipdb
import src.utils as utils
# import src.preprocessing as preprocessing
import src.feature_engineering as feature_engineering
import src.feature_selection as feature_selection

pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 8)

PROTOTYPING = False
use_feature_selection = True

param_regression = {
    'objective': 'regression_l2',
    "metric": 'rmse',
    'learning_rate': 0.005,
    'random_state': 1,
    'num_leaves': 50,
    # 'min_data_in_leaf': min_df,
    'max_depth': 10,
    # "feature_fraction": 0.9,
    "bagging_freq": 1,
    "bagging_fraction": 0.7,
    "lambda_l1": 0.1,
    "verbosity": -1,
}

param_classif = {
    'objective': 'binary',
    "metric": 'binary_logloss',
    'learning_rate': 0.005,
    'random_state': 1,
    'num_leaves': 50,
    # 'min_data_in_leaf': min_df,
    'max_depth': 10,
    # "feature_fraction": 0.9,
    "bagging_freq": 1,
    "bagging_fraction": 0.7,
    "lambda_l1": 0.1,
    "verbosity": -1,
}

lgb_wo_outliers = {
    'target': 'target',
    'param': param_regression,
    'drop_outliers': True,
    'fun_metric': utils.rmse,
    }

lgb_outliers = {
    'target': 'outliers',
    'param': param_classif,
    'drop_outliers': False,
    'fun_metric': utils.logloss,
    }

lgb_outliers_regression = {
    'target': 'outliers',
    'param': param_regression,
    'drop_outliers': False,
    'fun_metric': utils.logloss,
    }

lgb_single = {
    'target': 'target',
    'param': param_regression,
    'drop_outliers': False,
    'fun_metric': utils.logloss,
    }

dict_model = {}
dict_model['wo_outliers'] = lgb_wo_outliers
dict_model['outliers'] = lgb_outliers
dict_model['outliers_regression'] = lgb_outliers_regression
dict_model['single'] = lgb_single

df_learning, df_test, X_learning_sparse, X_test_sparse, X_cols_sparse = feature_engineering.prepare_data(PROTOTYPING)

to_drop = ['first_active_month', 'card_id', 'date_from', 'outliers', 'reference_date', 'target']

for model_name, model_config in dict_model.items():
    print('Detect feature correlation for dict_model: {}'.format(model_name))
    target = model_config['target']
    fun_metric = model_config['fun_metric']
    param = model_config['param']

    X_learning_dense = df_learning.drop(to_drop + [target], axis=1, errors='ignore')
    y_learning = df_learning[target]
    is_outlier_learning = df_learning['outliers']

    X_learning = X_learning_dense
    X_colnames = list(X_learning_dense)

    param = model_config['param']

    # Remove outliers
    if model_config['drop_outliers']:
        learning_wo = np.where(is_outlier_learning != 1)[0]
        X_learning = X_learning.iloc[learning_wo]
        y_learning = y_learning.iloc[learning_wo]

    cor = X_learning_dense.corr()
    cor.loc[:, :] = np.tril(cor, k=-1)
    cor = cor.stack()
    correlation_threshold = 0.97  # can be switched. Default value 0.99
    correlated = cor[cor > correlation_threshold].reset_index().loc[:, ['level_0', 'level_1']].copy()
    correlated = correlated.query('level_0 not in level_1')
    correlated_array = correlated.groupby('level_0').agg(lambda x: set(chain(x.level_0, x.level_1))).values

    l_correlated_array = []
    for i in correlated_array:
        l_correlated_array.append(list(i[0]))

    imp_df = feature_selection.get_feature_importances(
        X_learning=X_learning,
        y_learning=y_learning,
        X_colnames=list(X_learning),
        shuffle=False,
        param=model_config['param'])

    df_correlated = []
    for i, j in enumerate(l_correlated_array):
        df_correlated.append(pd.DataFrame({'feature': j, 'group': i}))

    df_correlated = pd.concat(df_correlated, sort=True)
    df_correlated = pd.merge(df_correlated, imp_df)
    df_correlated['rank'] = df_correlated.groupby('group')['importance_gain'].rank(ascending=False, method='first')
    df_correlated
    df_correlated = df_correlated[df_correlated['rank'] != 1].copy()
    correlated_features = df_correlated.feature.tolist()
    print('Removed {} correlated features'.format(len(correlated_features)))
    with open('feature_store/{}_correlated_features.pickle'.format(model_name), 'wb') as file:
        pickle.dump(correlated_features, file, protocol=pickle.HIGHEST_PROTOCOL)
