import numpy as np
import pandas as pd

from dateutil.relativedelta import relativedelta
from multiprocessing import Pool
from functools import partial

import hashlib

from sklearn.metrics import mean_squared_error, log_loss

import src.feature_selection as feature_selection
import pickle


def rmse(y_pred, y_true):
    return mean_squared_error(y_pred, y_true)**0.5


def logloss(y_pred, y_true):
    return log_loss(y_pred, y_true)


def reduce_mem_usage(df, verbose=True):
    numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
    start_mem = df.memory_usage().sum() / 1024**2
    for col in df.columns:
        col_type = df[col].dtypes
        if col_type in numerics:
            c_min = df[col].min()
            c_max = df[col].max()
            if str(col_type)[:3] == 'int':
                if c_min > np.iinfo(np.int8).min and c_max < np.iinfo(np.int8).max:
                    df[col] = df[col].astype(np.int8)
                elif c_min > np.iinfo(np.int16).min and c_max < np.iinfo(np.int16).max:
                    df[col] = df[col].astype(np.int16)
                elif c_min > np.iinfo(np.int32).min and c_max < np.iinfo(np.int32).max:
                    df[col] = df[col].astype(np.int32)
                elif c_min > np.iinfo(np.int64).min and c_max < np.iinfo(np.int64).max:
                    df[col] = df[col].astype(np.int64)
            else:
                if c_min > np.finfo(np.float16).min and c_max < np.finfo(np.float16).max:
                    df[col] = df[col].astype(np.float16)
                elif c_min > np.finfo(np.float32).min and c_max < np.finfo(np.float32).max:
                    df[col] = df[col].astype(np.float32)
                else:
                    df[col] = df[col].astype(np.float64)
    end_mem = df.memory_usage().sum() / 1024**2
    if verbose:
        print('Mem. usage decreased to {:5.2f} Mb ({:.1f}% reduction)'.format(end_mem, 100 * (start_mem - end_mem) / start_mem))
    return df


def complete_date(data, col_date='date',
                  col_id=['list_of_cols'],
                  freq='MS',
                  col_date_new=None):

    """
    If col_date_new is not None (a string) it means we want to keep col_date
    in order to do col_date_new - col_date to know how old is the value
    """

    if col_date_new is not None:
        name_col_date = col_date_new
    else:
        name_col_date = col_date

    # Aggregate data before using this function
    assert data[[col_date] + col_id].drop_duplicates().shape[0] == data.shape[0]

    distinct_groups = data[col_id].drop_duplicates()

    df_date_range = (pd.date_range(start=data[col_date].min(),
                     end=data[col_date].max(),
                     freq=freq).
                     rename(name_col_date).to_frame())

    df_complete = pd.merge(df_date_range.assign(key=1),
                           distinct_groups.assign(key=1),
                           on="key").drop("key", axis=1)

    # Cross-join - Complete missing observations for (date, model_id, platform_id)
    data_target = pd.merge(df_complete,
                           data,
                           how="left",
                           left_on=[name_col_date] + col_id,
                           right_on=[col_date] + col_id)

    return data_target


def rename_add(col, str_prepend='', str_append='', excepted=[]):
    if col not in excepted:
        col = str_prepend + col + str_append
    return col


def apply_func_before_date_from(data, func, window_month):
    """
    data : a pandas DataFrame
    we don't compute for all possible date_from but only the ones we are interested in
    func : function to apply to data
    window_month : a list for different rolling windows size

    TODO : add **kwargs arguments
    """
    res_window = []
    # date_from = data.purchase_date_month.max().to_period('M').to_timestamp()
    for w_m in window_month:
        res = []
        if w_m is not None:
            data = data[data['month_lag'] >= w_m]
        # date_begin = date_from - relativedelta(months=w_m)
        # tmp = data[data['purchase_date_month'] <= date_from].copy()
        # tmp = data[data['purchase_date_month'] >= date_begin].copy()
        data = func(data)
        res.append(data)

        res = pd.concat(res, axis=0)

        if w_m is not None:
            res = res.rename(lambda col: rename_add(col=col,
                                                    str_append='_m{}'.format(w_m),
                                                    excepted=['date_from', 'card_id']),
                             axis=1)
        res_window.append(res)

    from functools import reduce
    if len(res_window) > 1:
        res_window = reduce(lambda x, y: pd.merge(x, y, on=['card_id'], how='outer'), res_window)
    else:
        res_window = res_window[0]

    return res_window


# Based on Kaggle kernel by Scirpus
def convert_to_ffm(df, target, numerics, categories, fname="alltrainffm.txt"):
    currentcode = len(numerics)
    catdict = {}
    catcodes = {}
    for x in numerics:
        catdict[x] = 0
    for x in categories:
        catdict[x] = 1

    noofrows = df.shape[0]
    # noofcolumns = len(features)
    with open(fname, "w") as text_file:
        for n, r in enumerate(range(noofrows)):
            if((n % 100000) == 0):
                print('Row', n)
            datastring = ""
            datarow = df.iloc[r].to_dict()
            datastring += str(int(datarow[target]))

            for i, x in enumerate(catdict.keys()):
                if(catdict[x] == 0):
                    datastring = datastring + " "+str(i)+":" + str(i)+":" + str(datarow[x])
                else:
                    if(x not in catcodes):
                        catcodes[x] = {}
                        currentcode += 1
                        catcodes[x][datarow[x]] = currentcode
                    elif(datarow[x] not in catcodes[x]):
                        currentcode += 1
                        catcodes[x][datarow[x]] = currentcode

                    code = catcodes[x][datarow[x]]
                    datastring = datastring + " "+str(i)+":" + str(int(code))+":1"
            datastring += '\n'
            text_file.write(datastring)
    return None


def parallelize_function_dataframe(dataframe, function, num_cores=16, **kwargs):
    """
    Apply a function to a dataframe in a parallelized way
    TODO ajouter un argument partition by
    """
    # TODO permettre de prendre en entrée non pas un dataframe mais un grouped dataframe
    df_split = np.array_split(dataframe, num_cores*3)
    pool = Pool(num_cores)
    # [group for name, group in dfGrouped]
    dataframe = pd.concat(pool.map(partial(function, **kwargs), df_split))
    pool.close()
    pool.join()
    return dataframe


def str2hash(string):
    return int(hashlib.md5(string.encode('utf-8')).hexdigest()[:8], 16)


def parallelize_partition_function_dataframe(dataframe, function, partition_by=[], num_cores=16, **kwargs):
    """
    Apply a function to a dataframe in a parallelized way
    TODO ajouter un argument partition by
    """
    # TODO permettre de prendre en entrée non pas un dataframe mais un grouped dataframe
    pool = Pool(num_cores)
    if partition_by == []:
        df_split = np.array_split(dataframe, num_cores*3)
    else:
        hash_series = dataframe[partition_by].astype(str).sum(axis=1)
        dataframe['partition_by'] = hash_series.map(str2hash).mod(num_cores*3)
        grouped = dataframe.groupby('partition_by')
        df_split = [group for name, group in grouped]

    dataframe = pd.concat(pool.map(partial(function, **kwargs), df_split))
    pool.close()
    pool.join()

    return dataframe


def target_embedding(data, col, target):
    # Remove line where target is Na otherwise count values wouldn't be good
    data = data[~data[target].isnull()].copy()
    data[col] = data[col].fillna('dont_drop_na')
    new_col = '_'.join(col)
    # res = pd.DataFrame()
    grouped = data.groupby(col)
    res = grouped.agg({target: ['mean', 'count']})
    res.columns = [new_col + '_' + '_'.join(col).strip() for col in res.columns]
    res = res.reset_index()
    # res = grouped[target].mean().rename('{}_{}_mean'.format(new_col, target)).reset_index()
    # res['{}_{}_count'.format(new_col, target)] = grouped.size().values
    res = res.replace('dont_drop_na', np.nan)
    return res


def target_embedding_smooth(data, col, target, f=1, k=10, prior_col=None):
    # Copy/paste from target_embedding
    data = data[~data[target].isnull()].copy()
    data[col] = data[col].fillna('dont_drop_na')
    new_col = '_'.join(col)
    col_mean = '{}_{}_mean'.format(new_col, target)
    col_cnt = '{}_{}_count'.format(new_col, target)
    col_smoothed = '{}_{}_smooth_f{}_k{}'.format(new_col, target, f, k)
    res = pd.DataFrame()
    grouped = data.groupby(col)
    res = grouped[target].mean().rename(col_mean).reset_index()
    res[col_cnt] = grouped.size().values
    res = res.replace('dont_drop_na', np.nan)

    # Start easy, but there are different options for prior :
    # on all datapoints
    # mean of group (each group col have the same weight)
    # one of the columns of col, if there is a hierarchy (eg: cols = ['region', 'city'], prior can be based on the corresponding region)
    if prior_col is None:
        prior = data[target].mean()
        res['prior'] = prior
    else:
        # Not ready yet :(
        prior = data.groupby(prior_col)[target].mean().rename('prior').reset_index()
        prior = prior.fillna(data[target].mean())  # Safety
        res = pd.merge(res, prior)

    res['smooth'] = 1 / (1 + np.exp(-1 * (res[col_cnt] - k) / f))
    res[col_smoothed] = res['smooth'] * res[col_mean] + (1-res['smooth']) * res['prior']
    res = res[col + [col_smoothed]]

    return res


def oof_dataframe(data, col_fold, function, **kwargs):
    folders = data[col_fold].unique()
    res = pd.DataFrame()
    for f in folders:
        res_tmp = function(data[data[col_fold] != f], **kwargs)
        res_tmp[col_fold] = f
        res = res.append(res_tmp)

    return res


def adversarial_validation(X_train, X_test):
    from sklearn.model_selection import train_test_split
    import lightgbm as lgb
    import numpy as np
    import scipy
    from scipy.sparse import vstack
    if isinstance(X_train, pd.DataFrame):
        X_full = X_train.append(X_test)
    if isinstance(X_train, scipy.sparse.csr.csr_matrix):
        X_full = vstack([X_train, X_test], format='csr')

    y_full = np.concatenate((np.ones(X_train.shape[0]), np.zeros(X_test.shape[0])))

    X_train, X_valid, y_train, y_valid = train_test_split(X_full, y_full)

    param = {'objective': 'binary',
             "metric": 'auc',
             'learning_rate': 0.01}

    lgb_train = lgb.Dataset(X_train, label=y_train)
    lgb_valid = lgb.Dataset(X_valid, label=y_valid)

    model_gbm = lgb.train(param, lgb_train, 200,
                          valid_sets=[lgb_train, lgb_valid],
                          early_stopping_rounds=10,
                          verbose_eval=10)

    return model_gbm


def save_metric(df_metric, path='output/df_metric.csv'):
    df_metric['timestamp'] = pd.Timestamp.today().round("S")
    try:
        df_metric_old = pd.read_csv(path)
        df_metric = pd.concat([df_metric_old, df_metric], sort=True)
    except Exception as e:
        print(e)

    df_metric = df_metric.drop_duplicates()
    df_metric.to_csv(path, index=False)

    print('df_metric saved.')
    return None


def select_features(X_learning,
                    X_test,
                    model_name,
                    X_colnames,
                    use_feature_selection,
                    drop_correlated_features,
                    drop_sparse_merchant_id,
                    ):
    to_avoid_features = []
    if use_feature_selection:
        with open('feature_store/{}_null_features.pickle'.format(model_name), 'rb') as file:
            null_features = pickle.load(file)
        to_avoid_features = to_avoid_features + null_features
    if drop_correlated_features:
        with open('feature_store/{}_correlated_features.pickle'.format(model_name), 'rb') as file:
            correlated_features = pickle.load(file)
        to_avoid_features = to_avoid_features + correlated_features

    if model_name == 'wo_outliers':
        cols_refused = [i for i in X_colnames if i.startswith('refused_hist_')]
        to_avoid_features = to_avoid_features + cols_refused

    if drop_sparse_merchant_id:
        cols_sparse_merchant_id = [i for i in X_colnames if i.startswith('merchant_id_')]
        to_avoid_features = to_avoid_features + cols_sparse_merchant_id

    to_avoid_features = list(set(to_avoid_features))
    cols_usefull_position = feature_selection.get_position_usefull_importance_features(X_colnames, to_avoid_features)
    X_colnames = [X_colnames[i] for i in cols_usefull_position]

    X_learning = X_learning[:, cols_usefull_position]
    X_test = X_test[:, cols_usefull_position]

    return X_learning, X_test, X_colnames
