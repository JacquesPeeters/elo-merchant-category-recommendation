import pandas as pd
# import numpy as np

# from sklearn.model_selection import StratifiedKFold
from sklearn.linear_model import Ridge
# from sklearn.linear_model import LinearRegression

# import ipdb
import src.utils as utils # noqa
import src.preprocessing as preprocessing # noqa
import src.feature_engineering as feature_engineering

pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 8)

PROTOTYPING = False

df_learning, df_test = feature_engineering.prepare_l1_global_stacker()

to_drop = ['first_active_month', 'card_id', 'date_from', 'outliers',
           'reference_date', 'target', 'folder',
           ]
target = 'target'

X_learning = df_learning.drop(columns=to_drop, errors='ignore')
y_learning = df_learning[target]
is_outlier_learning = df_learning['outliers']

X_colnames = list(X_learning)
# X_test = df_test[X_colnames]
X_test = df_test[X_colnames]

df_metric = pd.DataFrame()

# model_ridge = LinearRegression()
model_ridge = Ridge()
model_ridge.fit(X_learning, y_learning)

print('Ridge l1_global:')
for col, coef in zip(list(X_learning), model_ridge.coef_):
    print(col, round(coef, 3))

col_pred = 'predict_l1_global'
df_learning[col_pred] = model_ridge.predict(X_learning)
df_test[col_pred] = model_ridge.predict(X_test)
score_ridge = round(utils.rmse(y_learning, df_learning[col_pred]), 4)
print('score_ridge: {}'.format(score_ridge))

df_metric = pd.DataFrame({'score_ridge': [score_ridge]})

df_predict = pd.DataFrame({
    col_pred: df_learning[col_pred],
    'card_id': df_learning['card_id']})

df_predict_test = pd.DataFrame({
    col_pred: df_test[col_pred],
    'card_id': df_test['card_id']})

df_predict = pd.concat([df_predict, df_predict_test], sort=True)

# df_predict.to_parquet('feature_store/level_2/df_predict.parquet.gzip', compression='gzip')
utils.save_metric(df_metric, path='output/level_1/df_metric_global.csv')

col_submit = col_pred
df_test['target'] = df_test[col_submit]
# Because we have one line by folder
df_test = df_test.groupby('card_id')['target'].mean().reset_index()

local_score = round(utils.rmse(y_learning, df_learning[col_submit]), 4)
message = "local_score: {}".format(local_score, 4)
print(message)
df_test[['card_id', 'target']].to_csv('output/submission.csv', index=False)

cmd_submit_kaggle = 'kaggle competitions submit -c elo-merchant-category-recommendation -f output/submission.csv -m "{} col_submit : {}"  '.format(message, col_submit)
print(cmd_submit_kaggle)
