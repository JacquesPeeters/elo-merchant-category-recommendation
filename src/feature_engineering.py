import pandas as pd
import numpy as np
import pickle

import src.utils as utils


def fe_card_auth(transactions):
    agg_fun = {'authorized_flag': ['mean', 'count']}
    card_auth = transactions.groupby(['card_id']).agg(agg_fun)
    card_auth.columns = ['_'.join(col).strip() for col in card_auth.columns.values]
    card_auth = card_auth.reset_index()
    return card_auth


def get_fe_repurchase(auth_hist_trans):
    fe_repurchase = auth_hist_trans[['card_id', 'merchant_id', 'merchant_category_id']].copy()
    fe_repurchase = fe_repurchase.groupby(['card_id', 'merchant_id', 'merchant_category_id']).size().rename('transac_cnt').reset_index()
    fe_repurchase['repurchase'] = (fe_repurchase['transac_cnt'] > 1).astype(int)
    fe_repurchase = utils.target_embedding(
        data=fe_repurchase,
        col=['card_id'],
        target='repurchase')
    return fe_repurchase


def fe_transactions_card(transactions):
    agg_func = {
        'card_id': ['count'],
        'category_1': ['mean', 'sum'],
        'category_2': ['nunique'],
        'category_3': ['nunique'],
        'category_2_1.0': ['mean', 'sum'],
        'category_2_2.0': ['mean', 'sum'],
        'category_2_3.0': ['mean', 'sum'],
        'category_2_4.0': ['mean', 'sum'],
        'category_2_5.0': ['mean', 'sum'],
        'category_2_nan': ['mean', 'sum'],
        'category_3_A': ['mean', 'sum'],
        'category_3_B': ['mean', 'sum'],
        'category_3_C': ['mean', 'sum'],
        'category_3_nan': ['mean', 'sum'],
        'category_4_0.0': ['mean', 'sum'],
        'category_4_1.0': ['mean', 'sum'],
        'category_4_nan': ['mean', 'sum'],
        'merchant_id': ['nunique'],
        'merchant_category_id': ['nunique'],
        'state_id': ['nunique'],
        'city_id': ['nunique'],
        'subsector_id': ['nunique'],
        'purchase_amount': ['sum', 'mean', 'max', 'min', 'std', 'skew'],
        'purchase_amount_raw': ['sum', 'mean', 'max', 'min', 'std', 'skew'],
        'installments': ['sum', 'mean', 'max', 'min', 'std', 'skew'],  # Probablement à suppr car déjà inclut dans category_3
        'purchase_date': [np.ptp, 'min', 'max'],
        'month_lag': ['mean', 'max', 'min', 'std', 'skew'],
        'month_diff': ['mean', 'max', 'min', 'std', 'skew'],
        'days_diff': ['mean', 'min', 'std', 'skew'],  # 'max' seems pure noise
        'automatic': ['mean', 'sum'],
        'nightclub': ['mean', 'sum'],
        'hour': ['mean'],
        'dow': ['mean'],
        }

    agg_func['price'] = ['sum', 'mean', 'max', 'min', 'var']
    agg_func['Christmas_Day_2017'] = ['mean']
    agg_func['Mothers_Day_2017'] = ['mean']
    agg_func['fathers_day_2017'] = ['mean']
    agg_func['Children_day_2017'] = ['mean']
    agg_func['Valentine_Day_2017'] = ['mean']
    agg_func['Black_Friday_2017'] = ['mean']
    agg_func['Mothers_Day_2018'] = ['mean']

    for col in ['category_2', 'category_3']:
        transactions[col+'_mean'] = transactions.groupby([col])['purchase_amount'].transform('mean')
        transactions[col+'_min'] = transactions.groupby([col])['purchase_amount'].transform('min')
        transactions[col+'_max'] = transactions.groupby([col])['purchase_amount'].transform('max')
        transactions[col+'_sum'] = transactions.groupby([col])['purchase_amount'].transform('sum')
        agg_func[col+'_mean'] = ['mean']

    transactions['purchase_date_rank'] = transactions.groupby('card_id')['purchase_date'].rank()
    cols = ['category_1']
    if transactions.authorized_flag.nunique() > 1:
        cols = cols + ['authorized_flag']
    for col in cols:
        transactions['{}_power'.format(col)] = transactions[col] * 2 ** -(transactions['purchase_date_rank'] - 1)
        agg_func['{}_power'.format(col)] = ['sum']

    fe_transactions_card = transactions.groupby(['card_id']).agg(agg_func)
    fe_transactions_card.columns = ['_'.join(col).strip() for col in fe_transactions_card.columns.values]
    fe_transactions_card = fe_transactions_card.reset_index()

    fe_transactions_card['purchase_date_ptp'] = fe_transactions_card['purchase_date_ptp'].dt.days

    fe_transactions_card['freq_while_active'] = fe_transactions_card['card_id_count'] / fe_transactions_card['purchase_date_ptp']
    fe_transactions_card['freq'] = fe_transactions_card['card_id_count'] / (fe_transactions_card['month_lag_min'].abs() + 1)
    # fe_transactions_card['card_active_freq'] = fe_transactions_card['card_transactions_size'] / (fe_transactions_card['card_month_lag_max'] - fe_transactions_card['card_month_lag_min'] + 1)

    fe_transactions_card['purchase_amount_raw_sum_purchase_date_ptp_rt'] = fe_transactions_card['purchase_amount_raw_sum'] / fe_transactions_card['purchase_date_ptp']

    # TODO revoir ici comment calculer la fréquence
    # fe_transactions_card['card_date_from_purchased_date_diff_max_freq'] = fe_transactions_card['card_date_from_purchased_date_diff_max'] / fe_transactions_card['card_transactions_size']
    # fe_transactions_card['card_date_from_purchased_date_diff_ptp'] = (fe_transactions_card['card_date_from_purchased_date_diff_max'] - fe_transactions_card['card_date_from_purchased_date_diff_min'])
    # fe_transactions_card['card_date_from_purchased_date_diff_ptp_freq'] = fe_transactions_card['card_date_from_purchased_date_diff_ptp'] / fe_transactions_card['card_transactions_size']

    agg_func = {}
    agg_func['purchase_amount_raw'] = ['sum']
    fe_transactions_card_month = transactions.groupby(['card_id', 'month_lag']).agg(agg_func)
    fe_transactions_card_month.columns = ['_'.join(col).strip() for col in fe_transactions_card_month.columns.values]
    fe_transactions_card_month['purchase_amount_raw_sum'] = fe_transactions_card_month['purchase_amount_raw_sum'].astype(float)
    fe_transactions_card_month = fe_transactions_card_month.reset_index()
    fe_transactions_card_month['month_lag'] = 'month_lag' + fe_transactions_card_month['month_lag'].map(str)

    fe_transactions_card_month = fe_transactions_card_month.pivot_table(index='card_id', columns='month_lag', values=['purchase_amount_raw_sum'])
    fe_transactions_card_month.columns = ['_'.join(col) for col in fe_transactions_card_month.columns]
    fe_transactions_card_month = fe_transactions_card_month.reset_index()

    fe_transactions_card = pd.merge(fe_transactions_card, fe_transactions_card_month)

    return fe_transactions_card


def fe_card_diff_pred_lag(card_diff_pred):
    """
    DEPRECATED - TO REMOVE
    """
    agg_func = {'diff_pred': ['mean', 'sum', 'min', 'max', 'std', 'count']}
    date_from_max = card_diff_pred['purchase_date_month'].max()
    card_diff_pred_lag = card_diff_pred.groupby(['card_id', 'purchase_date_month']).agg(agg_func)
    card_diff_pred_lag.columns = ['_'.join(col).strip() for col in card_diff_pred_lag.columns.values]
    card_diff_pred_lag = card_diff_pred_lag.reset_index()
    card_diff_pred_lag['date_from'] = date_from_max
    date_from = card_diff_pred_lag['date_from'].dt.year*12 + card_diff_pred_lag['date_from'].dt.month
    purchase_date_month = card_diff_pred_lag['purchase_date_month'].dt.year*12 + card_diff_pred_lag['purchase_date_month'].dt.month
    card_diff_pred_lag['lag_month'] = (date_from - purchase_date_month)
    card_diff_pred_lag = card_diff_pred_lag[card_diff_pred_lag['lag_month'] <= 6].copy()
    card_diff_pred_lag = card_diff_pred_lag.drop(columns=['date_from', 'purchase_date_month']).pivot(index='card_id', columns='lag_month')
    # NOT WORKINg, but date_from is dropped :(
    # tmp.drop(columns=['purchase_date_month']).pivot(index=['card_id', 'date_from'], columns='lag_month', values='diff_pred_mean')
    # tmp.columns = tmp.columns.to_series().str.join('_')
    card_diff_pred_lag.columns = ['lag_month_' + '_'.join([str(c) for c in col]).strip() for col in card_diff_pred_lag.columns.values]
    card_diff_pred_lag = card_diff_pred_lag.reset_index()
    card_diff_pred_lag['date_from'] = date_from_max

    return card_diff_pred_lag


def fun_fe_predict_transactions(predicted_transactions):
    predicted_transactions['not_outlier_pred'] = 1 - predicted_transactions['is_outlier_pred']
    agg_func = {'is_outlier_pred': ['mean', 'sum', 'min', 'max', 'std'],
                'not_outlier_pred': ['prod']}  # count is already contained in fe_card_auth()
    predicted_transactions = predicted_transactions.groupby('card_id').agg(agg_func)
    predicted_transactions.columns = ['_'.join(col).strip() for col in predicted_transactions.columns.values]
    predicted_transactions = predicted_transactions.reset_index()
    return predicted_transactions


def add_reference_date(hist_trans, train, test):
    card_reference_date = hist_trans[['card_id', 'reference_date']].drop_duplicates()
    n_train = len(train)
    n_test = len(test)
    train = pd.merge(train, card_reference_date)
    test = pd.merge(test, card_reference_date)
    assert n_train == len(train)
    assert n_test == len(test)

    train['card_age'] = (train['reference_date'] - train['first_active_month']).dt.days
    test['card_age'] = (test['reference_date'] - test['first_active_month']).dt.days

    return train, test


def fe_reference_date(train, test):
    # Assumes all train and test card_id are presents in hist_trans
    data = pd.concat([train, test], sort=True)
    # data['outliers'] = 0
    # data.loc[data['target'] < -30, 'outliers'] = 1

    N_FOLD = 5
    np.random.seed(0)
    data['fold'] = np.random.randint(N_FOLD, size=data.shape[0])

    fe_reference_date = data[['card_id', 'fold', 'reference_date', 'first_active_month']]
    for target in ['target']:  # , 'outliers'
        df_embed = utils.oof_dataframe(data=data,
                                       col_fold='fold',
                                       function=utils.target_embedding,
                                       col=['reference_date'],
                                       target=target
                                       )
        fe_reference_date = pd.merge(fe_reference_date, df_embed, how='left')

    fe_reference_date = fe_reference_date.drop(columns=['fold',
                                                        'reference_date',
                                                        'first_active_month'])
    return fe_reference_date


def get_learning(data,
                 feature_sets,
                 on=['card_id'],
                 PROTOTYPING=False):
    """
    data : A Datafame. Backbone dataset, each line is an entity we are trying to predict
    feature_sets : list of dataframes containing different granularity of feature engineering
    """
    df_learning = data
    nrow_begin = df_learning.shape[0]

    for df2merge in feature_sets:
        df_learning = pd.merge(df_learning, df2merge, on=on, how='left')

    nrow_end = df_learning.shape[0]

    if PROTOTYPING is False:
        assert nrow_begin == nrow_end
    return df_learning


def fe_learning(df_learning):
    df_learning['first_active_month_num'] = df_learning['first_active_month'].dt.year*12 + df_learning['first_active_month'].dt.month
    print('TODO - Améliorer ici')
    for col in ['auth_hist_purchase_date_min',
                'auth_hist_purchase_date_max',
                'new_purchase_date_min',
                'new_purchase_date_max']:
        df_learning['{}_first_active_month_diff'.format(col)] = (df_learning[col] -
                                                                 df_learning['first_active_month']).dt.days
        df_learning[col] = df_learning[col].astype(np.int64) * 1e-9  # Ugly

    for col in ['refused_hist_purchase_date_min', 'refused_hist_purchase_date_max']:
        df_learning[col] = df_learning[col].astype(np.int64) * 1e-9  # Ugly

    cols = ['purchase_amount_sum', 'purchase_amount_mean', 'purchase_amount_max',
            'purchase_amount_raw_sum', 'purchase_amount_raw_mean', 'purchase_amount_raw_max']
    for col in cols:
        df_learning['join_{}_sum'.format(col)] = df_learning['new_{}'.format(col)] + df_learning['auth_hist_{}'.format(col)]
        df_learning['join_{}_rt'.format(col)] = df_learning['new_{}'.format(col)] / df_learning['auth_hist_{}'.format(col)]
        df_learning['join_{}_diff'.format(col)] = df_learning['new_{}'.format(col)] - df_learning['auth_hist_{}'.format(col)]

    if 'target' in list(df_learning):
        # means is_train = True
        df_learning['outliers'] = 0
        df_learning.loc[df_learning['target'] < -30, 'outliers'] = 1

    fe_purchase_amount_raw_rt = df_learning.filter(regex='purchase_amount_raw_sum_month_lag').filter(regex='new|auth')
    fe_purchase_amount_raw_rt['card_id'] = df_learning['card_id']

    fe_purchase_amount_raw_rt = fe_purchase_amount_raw_rt.melt(id_vars=['card_id'])
    fe_purchase_amount_raw_rt = pd.merge(fe_purchase_amount_raw_rt, fe_purchase_amount_raw_rt.groupby('card_id')['value'].sum().rename('value_rt').reset_index())
    fe_purchase_amount_raw_rt['value_rt'] = fe_purchase_amount_raw_rt['value'] / fe_purchase_amount_raw_rt['value_rt']

    fe_purchase_amount_raw_rt['variable'] = fe_purchase_amount_raw_rt['variable'] + '_rt'

    fe_purchase_amount_raw_rt = fe_purchase_amount_raw_rt.pivot_table(index='card_id', columns='variable', values='value_rt')
    fe_purchase_amount_raw_rt = fe_purchase_amount_raw_rt.reset_index()

    df_learning = pd.merge(df_learning, fe_purchase_amount_raw_rt)

    return df_learning


def fe_learning_submodels(df_learning):
    df_learning['predict_outliers_regression'] = np.clip(df_learning['predict_outliers_regression'], 0, 1)
    df_learning['predict_outliers_stack'] = (df_learning['predict_outliers'] + df_learning['predict_outliers_regression'])/2
    df_learning['predict_outliers_stack_predict_wo_outliers_sum'] = (df_learning['predict_outliers_stack'] * (-33.21928095) + df_learning['predict_wo_outliers'])
    df_learning['predict_outliers_predict_wo_outliers_sum'] = (df_learning['predict_outliers'] * (-33.21928095) + df_learning['predict_wo_outliers'])
    df_learning['predict_outliers_predict_wo_outliers_sum'] = np.clip(df_learning['predict_outliers_predict_wo_outliers_sum'], -33.21928095, None)
    df_learning['predict_outliers_stack_predict_wo_outliers_sum'] = np.clip(df_learning['predict_outliers_stack_predict_wo_outliers_sum'], -33.21928095, None)

    return df_learning


def get_count_vectorizer(hist_trans, train, min_df):
    cols_to_countvectorize = ['city_id', 'merchant_category_id', 'merchant_id']  # , 'merchant_group_id']  # '', 'subsector_id'
    df_countvectorize = hist_trans[['card_id'] + cols_to_countvectorize].copy()

    for c in cols_to_countvectorize:
        df_countvectorize[c] = c + '_' + df_countvectorize[c].map(str)

    df_countvectorize = df_countvectorize.groupby('card_id').agg(lambda col: ' '.join(col)).reset_index()
    df_countvectorize['countvectorize'] = ''
    for c in cols_to_countvectorize:
        df_countvectorize['countvectorize'] = df_countvectorize['countvectorize'] + ' ' + df_countvectorize[c].map(str)

    df_countvectorize = pd.merge(df_countvectorize, train[['card_id']].eval('is_train = 1'), how='left')
    df_countvectorize['is_train'] = df_countvectorize['is_train'].fillna(0)

    from sklearn.feature_extraction.text import CountVectorizer
    count_vectorizer = CountVectorizer(min_df=min_df)
    # Fit only on training data, we won't legal learn anything from test data
    count_vectorizer.fit(df_countvectorize[df_countvectorize['is_train'] == 1]['countvectorize'])

    del df_countvectorize['is_train']

    return count_vectorizer, df_countvectorize


def stratify_train(train, random_state=120):
    # https://www.kaggle.com/c/elo-merchant-category-recommendation/discussion/78903#463667
    train['rounded_target'] = train['target'].round(0)
    train = train.sort_values('rounded_target').reset_index(drop=True)
    vc = train['rounded_target'].value_counts()
    vc = dict(sorted(vc.items()))
    df = pd.DataFrame()
    train['indexcol'], i = 0, 1
    for k, v in vc.items():
        step = train.shape[0]/v
        indent = train.shape[0]/(v+1)
        df2 = train[train['rounded_target'] == k].sample(v, random_state=120).reset_index(drop=True)
        for j in range(0, v):
            df2.at[j, 'indexcol'] = indent + j*step + 0.000001*i
        df = pd.concat([df2, df])
        i += 1
    train = df.sort_values('indexcol', ascending=True).reset_index(drop=True)
    del train['indexcol'], train['rounded_target']
    return train


def prepare_data(PROTOTYPING, random_state=120):
    test = pd.read_csv('data/test.csv', parse_dates=["first_active_month"])
    train = pd.read_csv('data/train.csv', parse_dates=["first_active_month"])

    authorized_flag = pd.read_parquet('feature_store/authorized_flag.parquet.gzip').reset_index(drop=True)
    fe_repurchase = pd.read_parquet('feature_store/fe_repurchase.parquet.gzip').reset_index(drop=True)
    new = pd.read_parquet('feature_store/new.parquet.gzip').reset_index(drop=True)
    # hist = pd.read_parquet('feature_store/hist.parquet.gzip').reset_index(drop=True)
    auth_hist = pd.read_parquet('feature_store/auth_hist.parquet.gzip').reset_index(drop=True)
    refused_hist = pd.read_parquet('feature_store/refused_hist.parquet.gzip').reset_index(drop=True)
    fe_reference_date = pd.read_parquet('feature_store/fe_reference_date.parquet.gzip').reset_index(drop=True)

    df_countvectorize = pd.read_parquet('feature_store/df_countvectorize.parquet.gzip').reset_index(drop=True)
    with open('feature_store/count_vectorizer.pickle', 'rb') as file:
        count_vectorizer = pickle.load(file)

    # df_countvectorize_hist = pd.read_parquet('feature_store/df_countvectorize_hist.parquet.gzip').reset_index(drop=True)
    # with open('feature_store/count_vectorizer_hist.pickle', 'rb') as file:
    #     count_vectorizer_hist = pickle.load(file)

    # with open('feature_store/min_df.pickle', 'rb') as file:
    #     min_df = pickle.load(file)

    # fe_predict_transactions = pd.read_csv('feature_store/fe_predict_transactions.csv')

    feature_sets = [
        authorized_flag,
        fe_repurchase,
        new,
        auth_hist,
        refused_hist,
        fe_reference_date,
      ]

    train = stratify_train(train, random_state=random_state)
    df_learning = get_learning(
        data=train,
        feature_sets=feature_sets,
        PROTOTYPING=PROTOTYPING)

    df_test = get_learning(
        data=test,
        feature_sets=feature_sets,
        PROTOTYPING=PROTOTYPING)

    df_learning = fe_learning(df_learning)
    df_test = fe_learning(df_test)

    # Reorder df_countvectorize according to df_learning and df_test
    df_countvectorize_learning = pd.merge(df_learning[['card_id']], df_countvectorize, how='left')
    df_countvectorize_test = pd.merge(df_test[['card_id']], df_countvectorize, how='left')
    assert (df_countvectorize_learning['card_id'] == df_learning['card_id']).all()
    assert (df_countvectorize_test['card_id'] == df_test['card_id']).all()
    X_learning_sparse = count_vectorizer.transform(df_countvectorize_learning['countvectorize'].fillna(''))
    X_test_sparse = count_vectorizer.transform(df_countvectorize_test['countvectorize'].fillna(''))
    X_cols_sparse = count_vectorizer.get_feature_names()

    return df_learning, df_test, X_learning_sparse, X_test_sparse, X_cols_sparse


def prepare_level1_stacker(model_name):
    import glob
    test = pd.read_csv('data/test.csv', parse_dates=["first_active_month"])
    train = pd.read_csv('data/train.csv', parse_dates=["first_active_month"])
    train = train.drop(columns=['feature_1', 'feature_2', 'feature_3'])
    test = test.drop(columns=['feature_1', 'feature_2', 'feature_3'])

    fname_df_predict = 'feature_store/machine/df_predict_{}_bayes*.parquet.gzip'.format(model_name)
    feature_sets = []
    for i, f in enumerate(glob.glob(fname_df_predict)):
        tmp = pd.read_parquet(f).reset_index(drop=True)
        col = 'predict_{}'.format(model_name)
        tmp = tmp.rename(columns={col: '{}_{}'.format(col, i)})
        feature_sets.append(tmp)

    df_learning = get_learning(
        data=train,
        feature_sets=feature_sets)

    df_test = get_learning(
        data=test,
        feature_sets=feature_sets)

    df_learning['outliers'] = 0
    df_learning.loc[df_learning['target'] < -30, 'outliers'] = 1

    return df_learning, df_test


def fe_learning_l2(df_learning):
    df_learning['predict_l1_outliers_regression'] = np.clip(df_learning['predict_l1_outliers_regression'], 0, 1)
    df_learning['predict_outliers_regression_wo_outliers_sum'] = (df_learning['predict_l1_outliers_regression'] * (-33.21928095) + df_learning['predict_l1_wo_outliers'])
    df_learning['predict_outliers_wo_outliers_sum'] = (df_learning['predict_l1_outliers'] * (-33.21928095) + df_learning['predict_l1_wo_outliers'])

    df_learning['predict_outliers_regression_wo_outliers_sum'] = np.clip(df_learning['predict_outliers_regression_wo_outliers_sum'], -33.21928095, None)
    df_learning['predict_outliers_wo_outliers_sum'] = np.clip(df_learning['predict_outliers_wo_outliers_sum'], -33.21928095, None)
    return df_learning


def prepare_level2_stacker():
    import glob
    test = pd.read_csv('data/test.csv', parse_dates=["first_active_month"])
    train = pd.read_csv('data/train.csv', parse_dates=["first_active_month"])
    train = train.drop(columns=['feature_1', 'feature_2', 'feature_3'])
    test = test.drop(columns=['feature_1', 'feature_2', 'feature_3'])

    fname_df_predict = 'feature_store/level_1/df_predict_*.parquet.gzip'
    feature_sets = []
    for i, f in enumerate(glob.glob(fname_df_predict)):
        tmp = pd.read_parquet(f).reset_index(drop=True)
        feature_sets.append(tmp)

    df_learning = get_learning(
        data=train,
        feature_sets=feature_sets)

    df_test = get_learning(
        data=test,
        feature_sets=feature_sets)

    df_learning['outliers'] = 0
    df_learning.loc[df_learning['target'] < -30, 'outliers'] = 1

    df_learning = fe_learning_l2(df_learning)
    df_test = fe_learning_l2(df_test)

    return df_learning, df_test


def prepare_l1_global_stacker():
    import glob
    test = pd.read_csv('data/test.csv', parse_dates=["first_active_month"])
    train = pd.read_csv('data/train.csv', parse_dates=["first_active_month"])
    train = train.drop(columns=['feature_1', 'feature_2', 'feature_3'])
    test = test.drop(columns=['feature_1', 'feature_2', 'feature_3'])

    fname_df_predict = 'feature_store/machine/df_predict_*_bayes*.parquet.gzip'
    feature_sets = []
    for i, f in enumerate(glob.glob(fname_df_predict)):
        tmp = pd.read_parquet(f).reset_index(drop=True)
        tmp = tmp.rename(columns={tmp.columns[-1]: tmp.columns[-1] + str(i)})
        feature_sets.append(tmp)

    df_learning = get_learning(
        data=train,
        feature_sets=feature_sets,
        on=None)

    df_test = get_learning(
        data=test,
        feature_sets=feature_sets,
        on=None,
        PROTOTYPING=True)

    df_learning['outliers'] = 0
    df_learning.loc[df_learning['target'] < -30, 'outliers'] = 1

    return df_learning, df_test
